<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privacy Policy</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                      <input type="text" class="form-control" placeholder="Search">
                      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                  </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container the_plot">
<div class="intro_com1">
  <h1>Cookie policy<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>On 8 May 2014, the Personal Data Protection Authority issued the provision n. 229, entitled "Identification of simplified procedures for the information and the acquisition of consent for the use of cookies", with subsequent publication in the Official Gazette on 3 June 2014. Through this provision, the Guarantor acknowledges the European directives according to the which each website, at the first access of a visitor, must show an informative banner that informs them of the cookie policy operated by the site they are visiting, and to make the continuation of navigation to the<br>
This cookie policy was updated on<span>24 May 2018 </span> . Any updates will always be published on this page.</p>
    </div>
  </div></div>
  <div class="container the_plot">
<div class="intro_com1">
  <h1>Definition<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>Cookies are small text strings that the sites visited by the user send to his terminal (usually the browser), where they are stored before being re-transmitted to the same sites at the next visit of the same user. During the navigation on a site, the user can also receive cookies on their terminal that are sent from different websites or web servers (so-called "third parties"), on which some elements may exist (such as, for example, images, maps, sounds, specific links to pages of other domains) on the site that the same is visiting.<br>
Cookies, usually present in users' browsers in very large numbers and sometimes even with long temporal persistence, are used for different purposes: execution of computer authentication, monitoring of sessions, storage of information on specific configurations concerning users who access the server, etc.<br><br>

The Guarantor, in order to reach a proper regulation of the directive, identified two broad categories of cookies: Technical cookies and profiling cookies ; and at the same time defined a further differentiation regarding the subject installing cookies, which may be the same site manager ("publisher") or a third party, which installs cookies through the first, defining this type of third-party cookies set off. In the latter case, the Guarantor has expressly limited the responsibility of the publisher to refer to the information of the party who installs these cookies.</p>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Technical<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>cookies Technical cookies are those used for the sole purpose of "transmitting a communication on an electronic communication network, or to the extent strictly necessary for the provider of an information society service explicitly requested by the subscriber or user to provide this service "(see Article 122, paragraph 1, of the Code).<br>
They are not used for other purposes and are normally installed directly by the owner or operator of the website. They can be divided into navigation or session cookies, which guarantee the normal navigation and use of the website (allowing, for example, to make a purchase or authenticate to access restricted areas); analytics cookies, similar to technical cookies when used directly by the site operator to collect information, in aggregate form, on the number of users and how they visit the site; functional cookies, which allow the<br><br>

For the installation of these cookies, the prior consent of users is not required, while the obligation to provide information pursuant to art. 13 of the Code</p>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Profiling<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>cookies<span> Profiling</span> cookies are designed to create profiles related to the user and are used in order to send advertising messages in line with the preferences expressed by the same in the context of surfing the net. Because of the particular invasiveness that such devices can have in the private sphere of users, European and Italian legislation requires the user to be adequately informed about their use and express their valid consent.<br><br>

The art. 122 of the Code whereby "the storage of information in the terminal device of a contractor or a user or access to information already filed is permitted only on condition that the contractor or the user has given his consent after being was informed with the simplified procedures referred to in Article 13, paragraph 3 "(Article 122, paragraph 1, of the Code).<br><br>

Our site, referring to the above, installs on the user's terminal only and first and third-party technical cookies including:<br><br>

Google Analytics - System of statistics<span> read the complete information </span>(modified in such a way as to anonymise the visit, so as to make the user's profiling impossible)<br>
Facebook - Social Media<span> read the full information</span><br>
YouTube - Video content<span> read the full information</span></p>
<h5><span>No personal data of users is acquired by the site in this regard</span></h5>
<h5><span>Check the cookie settings and turn them off</span></h5>
<p>Cookies facilitate and speed up the navigation on our site, you can still choose, if you wish, to refuse them. However, this will prevent us from remembering your favorites, offering you personalized content and collecting aggregated data from your visit to the site. You can change cookie settings directly from your web browser.<br><br>

To learn more about how to manage and delete cookies from your browser, go to the following links</p>
<ul class="cinema_box">
          <li>More information on disabling cookies on<span> Firefox</span></li>
          <li>Learn more about disabling cookies on<span> Chrome</span></li>
          <li>More information on disabling cookies on <span>Safari</span></li>
          <li>More information on disabling cookies on<span> Internet Explorer</span></li>
        </ul>
        <p>To learn more about how to manage and delete cookies from your browser, go to the following links</p>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Rights of the interested party <span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
        <p>Pursuant to art. 15 of the GDPR (Right to access personal data and other rights) of the Privacy Code, your rights regarding the processing of data are: </p>
<ul class="cinema_box">
          <li>to know, through free access, the existence of data processing that may concern you; </li>
          <li>be informed about the nature and purpose of the processing;</li>
          <li> Obtain from the holder, without delay:</li>
          <li>information about the existence of personal data, the origin of the same, the purposes and methods of treatment and, if present, to obtain access to  personal data and information referred to in Article 15 of the GDPR.</li>
          <li>request the updating, rectification, integration, deletion, limitation of data processing in the event one of the conditions provided for in Article 18 of the                  GDPR occurs, the transformation into anonymous form or blocking of personal data, processed in violation of the law, including those that do not need                to be kept for the purposes for which the data were collected and / or subsequently processed.
</li>
<li>object, in whole or in part, for legitimate reasons, to the processing of data, even if pertinent to the purpose of the collection and processing of
     personal data provided for the purposes of commercial information or sending advertising material or direct selling or for completion market research       or commercial communication. </li>
     <li>Each user also has the right to revoke the consent at any time without prejudice to the lawfulness of the treatment based on the consent given prior to         the revocation.</li>
     <li>receive personal data, provided knowingly and actively or through the use of the service, in a structured format, commonly used and readable by auto        matic device, and transmit them to another data controller without impediments. </li>
     <li>propose a complaint with the Italian Data Protection Authority.
</li>
        </ul>
        <p>The owner of the data processing for all legal purposes is: </p>
        <p><span>Mr. Marco Kohler</span></p>
        <p><span>E-mail: info@derfaust.it</span></p>
        <h5>By clicking on PROSEGUI , you consent to the use of cookies described above and to writing a technical cookie (valid for 12 months) to store this your choice and close this window.</h5>
    </div>
  </div>
</div>
<?php include 'footer.php';?>
</body>

</html>
