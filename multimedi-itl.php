<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Multimedia</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />

</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
             <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/multimedia1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">

 	<div class="row tb_box">
 		<div class="table-responsive">
		  <table class="table">
		    <thead>
		      <tr>
              <th>Scena</th>
            <th>Musica</th>
            <th>Clip</th>
            <th>Foto</th>
            <th>Grafica</th>
		      </tr>
		    </thead>
		    <tbody>
		      <tr>
		        
            <td>Prologo in Cielo</td>
		        <td><a href="#" data-toggle="modal" data-target="#clip1"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music1"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><img src="imago/grafica.png"></a></td>
		      </tr>
		      <tr>
		        
            <td>Ombra di Dio</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip2"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music"><!-- <img src="imago/clip.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica1"><img src="imago/grafica.png"></a></td>
		      </tr>
		      <tr>
		        
            <td>Festa di Piazza</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip3"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music2"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto1"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica2"><img src="imago/grafica.png"></a></td>
		      </tr>
		      <tr>
		        <td>Diavoli Violinisti</td>
		      <td><a href="#" data-toggle="modal" data-target="#clip4"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music3"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto2"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        
            <td>Konig im Thule</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip5"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music4"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto3"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        
            <td>Brucia come il Sale</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip6"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music5"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto4"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"></a> --></td>
		      </tr>
		      <tr>
		        
            <td>Salita al Brocken</td>
		      <td><a href="#" data-toggle="modal" data-target="#clip7"><img src="imago/music.png"></a></td>
		       <td><a href="#" data-toggle="modal" data-target="#music"><!-- <img src="imago/clip.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica6"><img src="imago/grafica.png"></a></td>
		      </tr>
		      <tr>
		        <td>In Fondo al Cuore</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip8"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music6"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto6"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        
            <td>Vieni con Me</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip9"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music7"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        
            <td>Libera la Testa</td>
		      <td><a href="#" data-toggle="modal" data-target="#clip10"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music8"><img src="imago/clip.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto7"><img src="imago/foto.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        
            <td>Io Sono un Uomo!</td>
		       <td><a href="#" data-toggle="modal" data-target="#clip11"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music"><!-- <img src="imago/clip.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica"><!-- <img src="imago/grafica.png"> --></a></td>
		      </tr>
		      <tr>
		        <td>La Pira</td>
		      <td><a href="#" data-toggle="modal" data-target="#clip12"><img src="imago/music.png"></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music"><!-- <img src="imago/clip.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#grafica11"><img src="imago/grafica.png"></a></td>
		      </tr>
		      <tr>
		        <td>Trailer</td>
		        <td><a href="#" data-toggle="modal" data-target="#music"><!-- <img src="imago/clip.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#music9"><img src="imago/clip.png"></a></td>

		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		        <td><a href="#" data-toggle="modal" data-target="#foto"><!-- <img src="imago/foto.png"> --></a></td>
		      </tr>
		    </tbody>
		  </table>
		</div>
 	</div>
</div>

<div id="music1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/prologocielo.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<div id="music2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/festadipiazza.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="music3" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/diavoliviolin.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="music4" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/konigimthule.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="music5" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/brucia.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="music6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/infondo.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="music7" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/vieniconme.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="music8" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/liberatesta.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="music9" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Clip</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <video width="100%" controls> <source src="media/02 video/shorttrailer.mp4" type="video/mp4"> 
              </video> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/01 Prologo in cielo.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/02 Ombra di Dio.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="clip3" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/03 Festa di piazza.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip4" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/04 Diavoli violinisti.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip5" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/05 Il Re di Thule.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/06 Brucia come il sale.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="clip7" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/07 Salita al Brocken.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="clip8" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/08 In fondo al cuore.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="clip9" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/09 Vieni con me.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip10" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/10 Io sono un uomo!.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip11" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/11 Libera la testa.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="clip12" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Musica</h4>
      </div>
      <div class="modal-body"> 
        <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
          <div class="carousel-inner"> 
            <div class="item active"> <audio width="100%" controls> <source src="media/01 musica/12 lapira.mp3" type="audio/mp3"> 
              </audio> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="foto" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel0" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel0" data-slide-to="0" class="active"><img src="media/03 immagini/foto/one/1.jpg"></li>
            <li data-target="#myCarousel0" data-slide-to="1"><img src="media/03 immagini/foto/one/2.jpg"></li>
            <li data-target="#myCarousel0" data-slide-to="2"><img src="media/03 immagini/foto/one/3.jpg"></li>
            <li data-target="#myCarousel0" data-slide-to="3"><img src="media/03 immagini/foto/one/4.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/one/01 Mefistofele1.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/one/02 Mefistofele.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/one/03 Mefistofele.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/one/03 Prologo in cielo .jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel0" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel0" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="foto1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel1" data-slide-to="0" class="active"><img src="media/03 immagini/foto/two/1.jpg"></li>
            <li data-target="#myCarousel1" data-slide-to="1"><img src="media/03 immagini/foto/two/2.jpg"></li>
            <li data-target="#myCarousel1" data-slide-to="2"><img src="media/03 immagini/foto/two/3.jpg"></li>
            <li data-target="#myCarousel1" data-slide-to="3"><img src="media/03 immagini/foto/two/4.jpg"></li>
            <li data-target="#myCarousel1" data-slide-to="4"><img src="media/03 immagini/foto/two/5.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/two/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/two/02.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/two/03.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/two/04.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/two/05.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel1" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel1" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="foto2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel2" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel2" data-slide-to="0" class="active"><img src="media/03 immagini/foto/three/1.jpg"></li>
            <li data-target="#myCarousel2" data-slide-to="1"><img src="media/03 immagini/foto/three/2.jpg"></li>
            <li data-target="#myCarousel2" data-slide-to="2"><img src="media/03 immagini/foto/three/3.jpg"></li>
            <li data-target="#myCarousel2" data-slide-to="3"><img src="media/03 immagini/foto/three/4.jpg"></li>
            <li data-target="#myCarousel2" data-slide-to="4"><img src="media/03 immagini/foto/three/5.jpg"></li>
            <li data-target="#myCarousel2" data-slide-to="5"><img src="media/03 immagini/foto/three/6.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/three/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/three/02.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/three/03.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/three/04.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/three/05.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/three/06.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel2" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel2" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="foto3" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel3" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel3" data-slide-to="0" class="active"><img src="media/03 immagini/foto/four/1.jpg"></li>
            <li data-target="#myCarousel3" data-slide-to="1"><img src="media/03 immagini/foto/four/2.jpg"></li>
            <li data-target="#myCarousel3" data-slide-to="2"><img src="media/03 immagini/foto/four/3.jpg"></li>
            <li data-target="#myCarousel3" data-slide-to="3"><img src="media/03 immagini/foto/four/4.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/four/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/four/02.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/four/03.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/four/04.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel3" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel3" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="foto4" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel4" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel4" data-slide-to="0" class="active"><img src="media/03 immagini/foto/five/1.jpg"></li>
            <li data-target="#myCarousel4" data-slide-to="1"><img src="media/03 immagini/foto/five/3.jpg"></li>
            <li data-target="#myCarousel4" data-slide-to="2"><img src="media/03 immagini/foto/five/4.jpg"></li>
            <li data-target="#myCarousel4" data-slide-to="3"><img src="media/03 immagini/foto/five/5.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/five/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/five/03.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/five/04.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/five/05.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel4" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel4" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>




<div id="foto6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel6" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel6" data-slide-to="0" class="active"><img src="media/03 immagini/foto/six/1.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="1"><img src="media/03 immagini/foto/six/2.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="2"><img src="media/03 immagini/foto/six/3.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="3"><img src="media/03 immagini/foto/six/4.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="4"><img src="media/03 immagini/foto/six/5.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="5"><img src="media/03 immagini/foto/six/6.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/six/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/six/02.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/six/03.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/six/04.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/six/05.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/six/06.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel6" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel6" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="foto7" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Foto</h4>
      </div>
      <div class="modal-body foto-slide"> 
        <div id="myCarousel7" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <li data-target="#myCarousel6" data-slide-to="0" class="active"><img src="media/03 immagini/foto/saven/1.jpg"></li>
            <li data-target="#myCarousel6" data-slide-to="1"><img src="media/03 immagini/foto/saven/2.jpg"></li>
          </ol>
          <div class="carousel-inner"> 
            <div class="item active"> <img src="media/03 immagini/foto/saven/01.jpg"></div>
            <div class="item"> <img src="media/03 immagini/foto/saven/02.jpg"></div>
          </div>
          <div class="popup_slider"> <a class="left carousel-control fa fa-angle-left" href="#myCarousel7" data-slide="prev"></a> 
            <a class="right carousel-control fa fa-angle-right" href="#myCarousel7" data-slide="next"></a></div>
        </div>
      </div>
    </div>
  </div>
</div>






<div id="grafica" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Grafica</h4>
      </div>
      <div class="modal-body"> <img src="media/03 immagini/grafica/graphica_img.jpg"> 
      </div>
    </div>
  </div>
</div>

<div id="grafica1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Grafica</h4>
      </div>
      <div class="modal-body"> <img src="media/03 immagini/grafica/ombra.jpg"> 
      </div>
    </div>
  </div>
</div>


<div id="grafica2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Grafica</h4>
      </div>
      <div class="modal-body"> <img src="media/03 immagini/grafica/festa.jpg"> 
      </div>
    </div>
  </div>
</div>

<div id="grafica6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Grafica</h4>
      </div>
      <div class="modal-body"> <img src="media/03 immagini/grafica/brocken.jpg"> 
      </div>
    </div>
  </div>
</div>

<div id="grafica11" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Grafica</h4>
      </div>
      <div class="modal-body"> <img src="media/03 immagini/grafica/vienicol.jpg"> 
      </div>
    </div>
  </div>
</div>
<?php include 'footer.php';?>

<script type="text/javascript">
		$('#clip1').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip2').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip3').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip4').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip5').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip6').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip7').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip8').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip8').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip9').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip10').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip11').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});
		$('#clip12').on('hidden.bs.modal', function () {
			$('audio').trigger('pause');
		});


//#music1 video

		$('#music1').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music2').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music3').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music4').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music5').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music6').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music7').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music8').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});
		$('#music9').on('hidden.bs.modal', function () {
			$('video').trigger('pause');
		});



</script>
</body>

</html>
