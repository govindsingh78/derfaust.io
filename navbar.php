<?php
session_start();
$lang = $_SESSION['lang'];
if ($lang == 'it') {

    $itl = "-itl";
} else {

    $itl = "";
}

$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];
?>

<style type="text/css">
        .form-align {
                clear: both;
                position: absolute;
                right: 40px;
        }
       .active{
            color: #f6cc33 !important;
        }
</style>

<?php
if ($lang == 'en') {
    ?>


<!-- Start Navigation Part for English -->
 <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand center-date" href="home<?php echo $itl; ?>.php?lang=en"><img src="imago/logo.png" class="logo" alt=""><span ><?=date("d F");?></span></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse sub_menu" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                            <li class="<?php if ($first_part == "home.php") {echo "active";}?>">
                                <a href="home<?php echo $itl; ?>.php?lang=en">Home</a>

                             <li class="dropdown <?php if ($first_part == "the-plot.php" || $first_part == "the_characters.php" || $first_part == "the-goethe.php") {echo "active";}?>">
                                <a href="the-plot<?php echo $itl; ?>.php?lang=en" class="dropdown-toggle" data-toggle="dropdown">The Opera</a>
                                <ul class="dropdown-menu">
                                    <li ><a href="the-plot<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "the-plot.php") {echo "active";}?>">The Plot</a></li>
                                    <li><a href="the_characters<?php echo $itl; ?>.php?lang=en"  class="<?php if ($first_part == "the_characters.php") {echo "active";}?>">The Characters</a></li>
                                    <li ><a href="the-goethe<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "the-goethe.php") {echo "active";}?>">Goethe</a></li>
                                </ul>
                            </li>

                            <li class="dropdown <?php if ($first_part == "thesources.php" || $first_part == "thesourcesliterature.php" || $first_part == "thesourcestheatre.php" || $first_part == "thesourcescinema.php" || $first_part == "thesourcesmusic.php" || $first_part == "thesourcespaintings.php") {echo "active";}?>">
                                <a href="thesources<?php echo $itl; ?>.php?lang=en" class="dropdown-toggle" data-toggle="dropdown">The Sources</a>
                                <ul class="dropdown-menu">
                                    <li ><a href="thesourcesliterature<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "thesourcesliterature.php") {echo "active";}?>">Literature</a></li>
                                    <li ><a href="thesourcestheatre<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "thesourcestheatre.php") {echo "active";}?>">Theatre</a></li>
                                    <li ><a href="thesourcescinema<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "thesourcescinema.php") {echo "active";}?>">Cinema</a></li>
                                    <li ><a href="thesourcesmusic<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "thesourcesmusic.php") {echo "active";}?>">Music</a></li>
                                    <li ><a href="thesourcespaintings<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "thesourcespaintings.php") {echo "active";}?>">Painting</a></li>
                                </ul>
                            </li>
                             <li class="dropdown <?php if ($first_part == "the-cast.php" || $first_part == "authors.php" || $first_part == "choreographies.php" || $first_part == "production.php") {echo "active";}?>">
                                <a href="the-cast<?php echo $itl; ?>.php?lang=en" class="dropdown-toggle" data-toggle="dropdown">The Show</a>
                                <ul class="dropdown-menu">
                                    <li ><a href="the-cast<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "the-cast.php") {echo "active";}?>">The Cast</a></li>
                                    <li ><a href="authors<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "authors.php") {echo "active";}?>">The Authors</a></li>
                                    <li ><a href="choreographies<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "choreographies.php") {echo "active";}?>">The Creatives</a></li>
                                    <li ><a href="production<?php echo $itl; ?>.php?lang=en" class="<?php if ($first_part == "production.php") {echo "active";}?>">The Production</a></li>
                                </ul>
                            </li>
                             <li class="<?php if ($first_part == "multimedi.php") {echo "active";}?>">
                                <a href="multimedi<?php echo $itl; ?>.php?lang=en">Multimedia</a>
                            </li>
                            <li class="<?php if ($first_part == "press-review.php") {echo "active";}?>">
                                <a href="press-review<?php echo $itl; ?>.php?lang=en">Press Review</a>
                            </li>
                             <!-- <li class="dropdown"><a href="#">Rassegna Stampa</a></li> -->
                            <li class="<?php if ($first_part == "contact.php") {echo "active";}?>"><a href="contact<?php echo $itl; ?>.php?lang=en">Contact Us</a></li>
                        <span class="lang_use pull-right" style="position: absolute;
right: 40px;">

                        <div class="btn-group">
                              <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ITA <img src="imago/ITL.png"> </button>
                              <div class="dropdown-menu language">
                                <ul>
                                <li><a id="en" class="dropdown-item" href="#" onclick="languageSwitcher(this.id)"> <img src="imago/lng_translate.png"> ENG</a></li>
                                <li><a id="it"  class="dropdown-item" href="#" onclick="languageSwitcher(this.id)"> <img src="imago/ITL.png"> ITA</a></li>
                                </ul>
                              </div>
                        </div>


                           <!--  <form action="" method="post" class="form-align">
                            <select name="set_language" id="custom-lang" onchange="languageSwitcher(this.value)">
                                    <option value="en" selected="selected">English</option>
                            <option value="it">Italiano</option>

                            </select>
                            </form> -->
                    </span>
                    </ul>

                </div><!-- /.navbar-collapse -->

            </div>

        </nav>
    </div>
    <!-- End Navigation -->
 <!-- End Navigation Part for English -->
 <?php
} else if ($lang == 'it') {
    ?>


<!-- Start Navigation Part for English -->
 <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand center-date" href="home<?php echo $itl; ?>.php?lang=it"><img src="imago/logo.png" class="logo" alt="">
                        <span> <?php
$date = date("d");
    $month = date("F");

    if ($month == 'January') {
        $monthItalian = 'gennaio';
    } elseif ($month == 'February') {
        $monthItalian = 'febbraio';
    } elseif ($month == 'March') {
        $monthItalian = 'marzo';
    } elseif ($month == 'April') {
        $monthItalian = 'aprile';
    } elseif ($month == 'May') {
        $monthItalian = 'Maggio';
    } elseif ($month == 'June') {
        $monthItalian = 'giugno';
    } elseif ($month == 'July') {
        $monthItalian = 'Luglio';
    } elseif ($month == 'August') {
        $monthItalian = 'Agosto';
    } elseif ($month == 'September') {
        $monthItalian = 'Settembre';
    } elseif ($month == 'October') {
        $monthItalian = 'Ottobre';
    } elseif ($month == 'November') {
        $monthItalian = 'Novembre';
    } elseif ($month == 'December') {
        $monthItalian = 'Dicembre';
    }
    $dateMonthinItalian = $date . " " . $monthItalian;
    echo $dateMonthinItalian;

    ?></span></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                 <div class="collapse navbar-collapse sub_menu" id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                            <li class="<?php if ($first_part == "home-itl.php") {echo "active";}?>">
                                <a href="home<?php echo $itl; ?>.php?lang=it">Home</a>

                             <li class="dropdown <?php if ($first_part == "the-plot-itl.php" || $first_part == "the_characters-itl.php" || $first_part == "the-goethe-itl.php") {echo "active";}?>">
                                <a href="the-plot<?php echo $itl; ?>.php?lang=it" class="dropdown-toggle" data-toggle="dropdown">L'opera</a>
                                <ul class="dropdown-menu">
                                    <li ><a class="<?php if ($first_part == "the-plot-itl.php") {echo "active";}?>" href="the-plot<?php echo $itl; ?>.php?lang=it">La trama</a></li>
                                    <li ><a class="<?php if ($first_part == "the_characters-itl.php") {echo "active";}?>" href="the_characters<?php echo $itl; ?>.php?lang=it">I personaggi</a></li>
                                    <li><a  class="<?php if ($first_part == "the-goethe-itl.php") {echo "active";}?>" href="the-goethe<?php echo $itl; ?>.php?lang=it">Goethe</a></li>
                                </ul>
                            </li>

                            <li class="dropdown <?php if ($first_part == "thesources-itl.php" || $first_part == "thesourcesliterature-itl.php" || $first_part == "thesourcestheatre-itl.php" || $first_part == "thesourcescinema-itl.php" || $first_part == "thesourcesmusic-itl.php" || $first_part == "thesourcespaintings-itl.php") {echo "active";}?>">
                                <a href="thesources<?php echo $itl; ?>.php?lang=it" class="dropdown-toggle" data-toggle="dropdown">Le fonti</a>
                                <ul class="dropdown-menu">
                                    <li ><a class="<?php if ($first_part == "thesourcesliterature-itl.php") {echo "active";}?>" href="thesourcesliterature<?php echo $itl; ?>.php?lang=it">Letteratura</a></li>
                                    <li ><a class="<?php if ($first_part == "thesourcestheatre-itl.php") {echo "active";}?>" href="thesourcestheatre<?php echo $itl; ?>.php?lang=it">Teatro</a></li>
                                    <li><a  class="<?php if ($first_part == "thesourcescinema-itl.php") {echo "active";}?>" href="thesourcescinema<?php echo $itl; ?>.php?lang=it">Cinema</a></li>
                                    <li ><a class="<?php if ($first_part == "thesourcesmusic-itl.php") {echo "active";}?>" href="thesourcesmusic<?php echo $itl; ?>.php?lang=it">Musica</a></li>
                                    <li ><a class="<?php if ($first_part == "thesourcespaintings-itl.php") {echo "active";}?>" href="thesourcespaintings<?php echo $itl; ?>.php?lang=it">Pittura</a></li>
                                </ul>
                            </li>
                             <li class="dropdown <?php if ($first_part == "the-cast-itl.php" || $first_part == "authors-itl.php" || $first_part == "choreographies-itl.php" || $first_part == "production-itl.php") {echo "active";}?>">
                                <a href="the-cast<?php echo $itl; ?>.php?lang=it" class="dropdown-toggle" data-toggle="dropdown">Lo spettacolo</a>
                                <ul class="dropdown-menu">
                                    <li ><a class="<?php if ($first_part == "the-cast-itl.php") {echo "active";}?>" href="the-cast<?php echo $itl; ?>.php?lang=it">Il cast</a></li>
                                    <li ><a class="<?php if ($first_part == "authors-itl.php") {echo "active";}?>" href="authors<?php echo $itl; ?>.php?lang=it">Gli autori</a></li>
                                    <li><a  class="<?php if ($first_part == "choreographies-itl.php") {echo "active";}?>" href="choreographies<?php echo $itl; ?>.php?lang=it">I Creativi</a></li>
                                    <li><a  class="<?php if ($first_part == "production-itl.php") {echo "active";}?>" href="production<?php echo $itl; ?>.php?lang=it">La produzione</a></li>
                                </ul>
                            </li>
                             <li class="<?php if ($first_part == "multimedi-itl.php") {echo "active";}?>">
                                <a href="multimedi<?php echo $itl; ?>.php?lang=it">Multimedia</a>
                            </li>
                            <li class="<?php if ($first_part == "press-review-itl.php") {echo "active";}?>">
                                <a href="press-review<?php echo $itl; ?>.php?lang=it">Rassegna stampa</a>
                            </li>
                             <!-- <li class="dropdown"><a href="#">Rassegna Stampa</a></li> -->
                            <li class=' <?php if ($first_part == "contact-itl.php") {echo "active";}?> '><a href="contact<?php echo $itl; ?>.php?lang=it">Contatti</a></li>
                       <!--  <span class="lang_use pull-right">
                        <div class="btn-group">
                              <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ENG <img src="imago/lng_translate.png"> </button>
                              <div class="dropdown-menu language">
                                <ul>
                                <li><a class="dropdown-item" href="#"> <img src="imago/lng_translate.png"> ENG</a></li>
                                <li><a class="dropdown-item" href="#"> <img src="imago/ITL.png"> ITA</a></li>
                                </ul>
                              </div>
                        </div>
                    </span> -->
                    <span class="lang_use pull-right" style="position: absolute;
right: 40px;">
                      <div class="btn-group">
                              <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ENG <img src="imago/lng_translate.png"> </button>
                              <div class="dropdown-menu language">
                                <ul>
                                <li><a id="en" class="dropdown-item" href="#" onclick="languageSwitcher(this.id)"> <img src="imago/lng_translate.png"> ENG</a></li>
                                <li><a id="it"  class="dropdown-item" href="#" onclick="languageSwitcher(this.id)"> <img src="imago/ITL.png"> ITA</a></li>
                                </ul>
                              </div>
                        </div>
  </span>
                  <!--   <form class="form-align" action="" method="post" >
                            <select name="set_language" id="custom-lang" onchange="languageSwitcher(this.value)">
                            <option value="it" selected="selected" data-thumbnail="images/ITL.png"><img src=""> Italiano</option>
                            <option value="en" data-thumbnail="images/lng_translate.png"> <img src=""> English</option>
                            </select>
                            </form> -->
                    </ul>

                </div><!-- /.navbar-collapse -->

            </div>

        </nav>
    </div>
    <!-- End Navigation -->
 <!-- End Navigation Part for English -->

 <?php
}
?>