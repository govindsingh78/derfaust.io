<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Characters</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/theopera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container performer-box">
<div class="intro_com1">
    <h1>Characters<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/faust.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>FAUST</h3>
      <p>Doktor Faust or Doctor Faustus , is the protagonist of the homonymous work by J.W.Goethe, based in turn on a German folk tale and on the vision of the German scholar of a puppet theater (Puppenspiele). The story concerns the fate of Faust, an alchemist, who, in his continuous search for advanced and forbidden knowledge of material things, invokes the devil (represented by Mephistopheles), who offers to serve him, giving him absolute knowledge, and an eternal youth for a period of time equal to 24 years, after which he will be master of his soul. <br><br>

Goethe changes this popular story by changing the pact in a bet, which however Mephistopheles is sure to win. This literary character has a definitely historical reference.<br>
Some testimonies around the sixteenth century, speak of a certain Giovanni Faust (born in Heidelberg in 1480) who appears in various German cities claiming to possess thaumaturgical powers and profound knowledge of occult doctrines. For the contemporaries, this figure possesses devilish attributes, so much so that Melanchthon, a humanist, a German theologian, as well as a personal friend of Martin Luther, calls him "turpissima bestia et cloaca multorum
diabolorum".</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/mefl.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MEPHISTOPHELES</h3>
      <p>Demon, who proposes himself to the old Doktor Faust, proposing him a bet in exchange for youth, infinite knowledge and unlimited power. Paradoxically, it is a character almost more "human" than Faust, which instead has a "titanic" and "insatiable" ambition of knowledge and power. <br><br>

The bet that Mephistopheles will lose, concerns the search for the Faust of the unrepeatable moment of maximum beauty. Only if the doctor admits he has found this splendid and unrepeatable moment, then Mephistopheles will have his soul ... this will not happen and Faust will have saved the soul.<br><br>

Note that Mephistopheles as a demon can not do good even when it should and therefore always inserts something "demonically" wrong in the positive things it does. An example is the poisoning of Margherita's mother; if he had wanted to make Faust happy and bring him to that required satisfaction, he would never have to give a poison instead of the sleeping pill. Here his evil nature turns against his own interests.</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marg.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MARGHERITA</h3>
      <p>It is the girl loved and seduced by Faust, who first involuntarily kills her mother with a poison passed off as a sleeping aid given to him by Faust to make love undisturbed.<br><br>
      Abandoned by Faust, after the death of his brother Valentino, kills the child conceived with Faust and is sentenced to death.<br><br>
      Faust tries to free her from jail, but she senses the demonic presence of Mephistopheles rejects freedom.<br><br>
      Thanks to this choice God saves his soul.</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marta.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MARTA</h3>
      <p>Marta is Margherita's "elderly" friend, whom she brings to show the jewels found in her bedroom.<br>
Marta tries them and struts, trying some of them. <br>
She was recently a widow, with her husband having eaten all her belongings in war, leaving her alone and poor, she is a woman no longer young, looking for a new "arrangement".<br><br>

Mephistopheles who had communicated the "sad" news of her husband's death, immediately became her goal to be able to marry again. <br><br>

In the scene of the "Garden of Martha" we witness the fun duet between her and Mephistopheles, which also puts the Devil in serious trouble! Even the Devil escapes when there is a woman of this type!<br>

Mephistopheles deceives her on a presumed interest, but in reality she takes time to allow Faust to know and seduce Margaret. After this scene we will not see her again ...</p>

    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/valen.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>VALENTINO</h3>
      <p>Brother of Margherita<br><br>
      He is a soldier who returns from the war and is concerned with safeguarding his sister's honor.<br><br>
     Mefistopheles meets him in an inn and says that his sister is not that good girl to boast about; to prove it, he invites him to return home to see for himself.<br><br>
     Valentino arrives home and sees Faust come out of it.<br><br>
     He confronts Faust in a duel, but Faust, helped by the diabolical arts of Mephistopheles, kills him and runs away.
</p>
    </div>
  </div>
    <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/wagner.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>WAGNER</h3>
      <p>He is a disciple of Faust, young and petulant who is already convinced that he has understood and knows almost everything there is to know.<br><br>
      Faust, seized by his scomforto, after the clash with the Spirit of the Earth, barely bears him when he visits him in his studio at night.<br><br>
      With him Faust is confident, after the Easter Festival, about the "miraculous" medicine of his father, used to treat the plague;this had killed more than he had saved!<br><br>
      In the work he reappears in the second part ("zweiter teil"), during the creation of "Homunculus" which has not been included in the musical.
</p>
    </div>
  </div>
</div>

<?php include 'footer.php';?>
</body>

</html>
