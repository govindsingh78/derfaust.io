<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Authors</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/show1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
    <h1>The Composer<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/roberto.jpg"></span> 
      <h4>Roberto CHIOCCIA</h4>
    </div>
    <div class="characters_right_box">
      <h3>Rome <abbr>(1963)</abbr></h3>
      <p>His professional figure is formed through a complete artistic preparation, bothfigurative and specifically musical. <br><br>

The artistic high school and the conservatory of S.Cecilia in Rome, where he attended the courses of piano,
composition and clarinet, are the places where he soon manifests that marked dowry of sensibility that distinguishes him in the current development of his work.<br><br>

In the years between 1980 and 1990, he actively participated in orchestral and band music, which, together with the experiences of music acquired live, become the raw material of that timbric palette to which a musician draws for life. <br><br>

Composer SIAE since 1982, has to his credit classical and modern music products already published and distributed by various record labels.<br><br>
He writes music for soundtracks of documentaries and films: • "Ultima Maremma" by A.Bàssan -<span> Silver Ribbon Award - Taormina 1983</span></p>
<ul class="cinema_box">
          <li>"Scratch and win" Dània Film 1998 - ed. EMI</li>
          <li>Documentary "I trabucchi" - production Gargano National Park</li>
        </ul>
        <p>In theater collaborates for variety shows (Teatro delleattrattive 1987) and for musical comedies (Teatro della Cometa, Teatro Sala Umberto). <br><br>

As a musician arranger follows many artists in the realization of their CDs, taking care of the orchestration, the studio recordings and the entire creative path.</p>
    </div>
  </div>
  <div class="intro_com1">
    <h1>The Libretist<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/aless.jpg"></span> 
      <h4>Alessandro Hellmann</h4>
    </div>
    <div class="characters_right_box">
      <h3>Genoa <abbr>(1971)</abbr></h3>
      <p>Hellmann has earned the status of cult author, ranging from literature to music and theater with works of rare intensity and consistency.<br><br>
His literary work describes the point of view of the weakest with an humorous and very personal style, made up of sudden lyric verticalizations, always in balance between the dramatic register and the grotesque.<br><br>
His literary work includes, among the others, titles as <span>“JUSTUS” (2017)</span>, and <span>“DAVID LAZZARETTI” (2013)</span>, source of inspiration for the theatre piéce by <b>Simone Cristicchi</b>, <span>“CUBA” (2008)</span> and the multi award-winning <span>"CENT’ANNI DI VELENO” (2005)</span>, published in France as <span>“LE FLEUVE PILLÉ”</span>, reviewed by national press as <b>"a small masterpiece of narrative technique"</b> and <b>"a text of high literary and human value"</b>.<br><br>

He also writes for theater and collaborates with several musicians and emerging groups as a songwriter and recorded a couple of albums: <span>“SUMMERTIME BLUE” (Trelune, 2008)</span> and <span>“COME PRATI A PRIMAVERA” (Maremosso, 2019)</span><br><br>

He has received several awards including the prestigious <span>PREMIO FABRIZIO DE ANDRÉ</span> <b>“to the best author”</b>, conferred by <b>DORI GHEZZI</b>.</p><br><br>

    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
