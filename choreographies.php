<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Choreographies</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                      <input type="text" class="form-control" placeholder="Search">
                      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                  </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/show1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
  <h1>Choreographies<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_left_box">
     <span>
      <img src="imago/sreafono.jpg"></span>
      <h4>Stefano Bontemp</h4>
    </div>
    <div class="characters_right_box">
      <h3>Rome <abbr>(1970)</abbr></h3>
     <h5>He graduated from the National Academy of Classical Dance in Rome and
worked in Theater as a performer in:</h5>
<ul class="cinema_box">
          <li><span>The Star Company of Hamburg</span> (Cast) </li>
          <li><span>The Company of the Rancia</span> (La cage aux follles, Cabaret)</li>
          <li><span>With G.Guidi and MLBaccarini</span> (Taxi with 2 squares, Promesse, promesse, Chicago)</li>
        </ul>
        <h5>He has signed several choreographies, including:</h5>
<ul class="cinema_box">
          <li>They are playing our song</li>
          <li>Promises, promises</li>
          <li>Gianburrasca</li>
          <li>Fame</li>
          <li>Deep Red</li>
          <li>If I'm here tonight <span>Loretta Goggi.</span> </li>
          <h5><span>Choreographer Assistant:</span> Ursula DE NITTIS</h5>
        </ul>
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>
</html>
