<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>I Personaggi</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/la_opera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container performer-box">
<div class="intro_com1">
    <h1>I personaggi<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/faust.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>FAUST</h3>
      <p>Doktor Faust o Doctor Faustus, è il protagonista dell'opera omonima di J.W.Goethe, basata a sua volta su un racconto popolare tedesco e sulla visione del letterato tedesco di un teatrino dei burattini (Puppenspiele). <br>
Il racconto riguarda il destino di Faust, un alchimista, il quale, nella sua continua ricerca di conoscenze avanzate e proibite delle cose materiali, invoca il diavolo (rappresentato da Mefistofele), che si offre di servirlo, donandogli la conoscenza assoluta, e una eterna giovinezza per un periodo di tempo pari a 24 anni, terminato i quali sarà padrone della sua anima.
<br> <br>
Goethe modifica questa storia popolare, cambiando il patto in una scommessa, che comunque Mefistofele è sicuro di vincere. Questo personaggio letterario ha un riferimento sicuramente storico.<br>
Infatti, alcune testimonianze intorno al XVI° secolo, parlano di un certo Giovanni Faust (nato a Heidelberg nel 1480) che appare in varie città tedesche affermando di possedere potenze taumaturgiche e la profonda conoscenza delle dottrine occulte. Per i contemporanei, questa figura possiede attributi diabolici, tanto che Melantone, umanista, teologo tedesco, oltre che amico personale di Martin Lutero, lo chiama «turpissima bestia et cloaca multorum diabolorum».</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/mefl.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MEFISTOFELE</h3>
      <p>Demone, che si propone al vecchio Doktor Faust, proponendogli una scommessa in cambio di giovinezza, conoscenza infinita e potere illimitato. Paradossalmente è un personaggio quasi più “umano” di Faust, che invece possiede un'ambizione “titanica” e “insaziabile” di conoscenza e potere.
<br>
<br>
La scommessa che Mefistofele perderà, riguarda la ricerca di Faust dell'ATTIMO IRRIPETIBILE di bellezza massima. Solo se il dottore ammetterà di aver trovato quest'attimo splendido e irripetibile, allora Mefistofele avrà la sua anima.. questo non avverrà e Faust avrà salva l'anima.
<br>
<br>
Si noti che Mefistofele in quanto demone non riesce a fare del bene anche quando dovrebbe e quindi inserisce sempre qualcosa di “demonicamente” sbagliato nelle cose positive che fa. Un esempio è l'avvelenamento della madre di Margherita; se avesse voluto far felice Faust e portarlo verso quell'appagamento richiesto, non avrebbe mai dovuto dare un veleno al posto del sonnifero. Ecco che la sua natura malvagia si rivolge contro i suoi stessi interessi.</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marg.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MARGHERITA</h3>
      <p>È la fanciulla amata e sedotta da Faust, che prima involontariamente uccide la madre con un veleno spacciato per sonnifero datogli da Faust per fare l'amore indisturbati.
<br>
<br>
Aabbandonata da Faust, dopo la morte del fratello Valentino, uccide il bimbo concepito con Faust ed è condannata a morte.
<br>
<br>
Faust cerca di liberarla dal carcere, ma lei sentendo la presenza demoniaca di Mefistofele rifiuta la libertà.
<br>
<br>
Grazie a questa scelta Dio salva la sua anima.</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marta.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>MARTA</h3>
      <p>Marta è l'amica "attempata" di Margherita, alla quale lei porta a far vedere i gioielli trovati nella sua camera da letto.<br>
Marta li prova e si pavoneggia, provandone alcuni. Rimasta vedova da poco, col marito che si è mangiato tutti i suoi averi in guerra, lasciandola sola e povera, è una donna non più giovane, alla ricerca di una nuova "sistemazione".
<br>
<br>
Mefistofele che aveva comunicato la "triste" notizia della morte del marito, diventa subito un suo obiettivo per potersi nuovamente maritare.
<br>
<br>
Nella scena del "Giardino di Marta" assistiamo al divertente duetto tra lei e Mefistofele, che mette in seria difficoltà anche il Diavolo! Anche il Diavolo scappa quando c'è una donna di questo tipo!
<br>
Mefistofele la illude su un suo presunto interesse, ma in realtà prende tempo per consentire a Faust di conoscere e sedurre Margherita.<br>
Dopo questa scena non la vedremo più...</p>
    </div>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/valen.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>VALENTINO</h3>
      <p>Fratello di Margherita<br><br>
   &Egrave; un soldato che torna dalla guerra e si preoccupa di salvaguardare l'onore della sorella.
<br><br>
Mefistofele lo incontra in un'osteria e afferma che la sorella non è quella brava ragazza della quale vantarsi; per dimostrarlo lo invita a ritornare a casa per vedere con i propri occhi.
<br><br>
Valentino arriva a casa e vede Faust uscirne.
<br><br>
Affronta Faust in duello, ma Faust aiutato dalle arti diaboliche di Mefistofele lo uccide e scappa via.
</p>
    </div>
  </div>
    <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/wagner.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>WAGNER</h3>
      <p>È un discepolo di Faust, giovane e petulante che è già convinto di aver capito e di conoscere quasi tutto quello che c'è da conoscere.
<br><br>
Faust, preso dal suo scomforto, dopo lo scontro con lo Spirito della Terra, lo sopporta a mala pena quando lo va a trovare nel suo studio, di notte.
<br><br>
Con lui Faust si confida, dopo la Festa di Pasqua, riguardo la medicina "miracolosa" del padre, usata per curare la peste; questa ne aveva ammazzati più di quanti ne avesse salvati!
<br><br>
Nell'opera ricompare nella seconda parte ("zweiter teil"), durante la creazione di "Homunculus" che nel musical non è stata inserita.
</p>
    </div>
  </div>
</div>

<?php include 'footer.php';?>
</body>

</html>
