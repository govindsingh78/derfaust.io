<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Goethe</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/theopera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container the_plot">
<div class="intro_com1">
    <h1>Goethe<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/goethe.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>JW Goethe <abbr>(1749 - 1832)</abbr></h3>
     <p><span>Johann Wolfgang von Goethe</span> , poet, playwright, novelist and German scientist, was born in<span> Frankfurt in 1749</span> , son of an official of the imperial administration; at the age of 16 he left Frankfurt to study law in Leipzig and then in Strasbourg.<br><br>
     Those were years of intense social and cultural life; he became interested in medicine, figurative arts, drawing, and began writing libertine and joking verses (The Book of Annette). From the same period is a tragedy in verse, I Complici (1768), followed by the dramas Goetz von Berlichingen (1771), Clavigo and Stella.<br><br>

At the break of the short romance with Käthchen Schömkopf followed a phase of disturbance and agitation: then in 1768, seriously ill, he returned to Frankfurt and, after the critical phase of the disease, during the convalescence he devoted himself to studies of magic, astrology, alchemy.<br><br>

From 1770 to 1771 he lived in Strasbourg, where, alongside legal disciplines, he cultivated the study of music, art, anatomy and chemistry; here he had the revelation of Gothic art through Herder, and fell in love with<span> Friederike Brion</span> ,<br><br>
daughter of the Protestant pastor of Sesenheim. The joy and the tensions of that love inspired him some of the most beautiful lyrics of this period, while the feeling of guilt following the abandonment of Friederike will become, transposed, that of Faust towards Margherita. <br>
Between May and September 1771 Goethe had been in Wetzlar as a practitioner in the court. There he had fallen in love with<span> Charlotte Buff</span> ; and back to Frankfurt, he transposes that unattainable love in the epistolary novel The Sorrows of the Young Werther, which caused great scandal but conquered an international success in Goethe.<br><br>

In 1775 he moved to the Weimar court, where he became a councilor of state. The love for<span> Charlotte von Stein</span> and the international success of her works (in particular the scandal aroused by Werther) made Goethe the undisputed ruler of the German literary scene. In 1775 Goethe traveled to Switzerland with the Stolberg brothers and went as far as the Gotthard, attracted by Italy. Back in Frankfurt, he broke off his engagement with Schönemann.<br><br>

In Strasbourg he had two meetings that would have been very important in his life and decisive for his literary work. The first was the one with Friederike Brion, daughter of a Protestant pastor whom Goethe loved and who would provide the model for various of his female characters. The second was the meeting with the philosopher and literary critic Johann Gottfried von Herder with whom he became friends: Herder, among other things, led him to escape the influence of French classicism, and introduced him to the work of Shakespeare, in which just the failure to respect traditional units contributes to the dramatic intensity.<br><br>

Herder, moreover, induced Goethe to deepen the meaning of German popular poetry and forms of Gothic architecture as sources of literary inspiration. <br><br>

The work, which takes Shakespeare's model, has as its protagonist a knight of the sixteenth century in revolt against the authority of the emperor and the church, and anticipates the libertarian thrills that would have been the soul of the Sturm und Drang movement, a forerunner of romanticism German.<br><br>

On returning to Weimar (1788) Goethe found a hostile atmosphere in literary circles, while at court he never accepted his relationship with Christiane Vulpius, a young woman who in 1789 gave him a son and whom he would marry in 1806.<br><br>

In spite of everything, he remained in Weimar, held back by two motives of interest: the direction of the ducal theater, which he held from 1791 to 1813, and the possibility of pursuing scientific studies better than elsewhere, to which he dedicated himself with renewed ardor. Several years of comparative anatomy (1784), of botany (1790) and two volumes of optics (1791 and 1792) date back to these years.<br><br>

It was<span> friendship with Friedrich von Schiller</span> to bring Goethe closer to literature and from their collaboration, which lasted from 1794 to Schiller's death in 1805, resulted in numerous lyrical and epic compositions, the hexameter's idyll Arminio and Dorotea (1797), the drama La natural daughter (1802), the second version of the novel The Years of Novitiate by Wilhelm Meister (1796), which would have constituted a narrative model for the subsequent German literary production, inaugurating the genre of the training novel and above all, on the encouragement of Schiller, the final version of Faust (the first part was published in 1808).<br><br>

<span>From 1805 until his death Goethe lived years of intense creativity</span> . The great historical events of his time found in him a careful but not passionate observer: the French revolution was considered by him with a certain suspicion, not so much as an expression of an instance of freedom, but as the uncontrolled outbreak of dark and disordered forces ; he admired Napoleon, as he suspected the prospect of German unification.<br><br>

Among the writings of these years we must mention the novel<span> Elective Affinities (1809)</span> , the autobiography, the collection of lyrics Poetry and Truth,<span> The Western-Eastern Sofa (1819)</span> , with mystical and erotic, licentious and ambiguous tones;<span>the second part of FAUST (published posthumously in 1832)</span>  .<br><br>

On March 17, he wrote to Wilhelm von Humboldt: "<span> I have been thinking about FAUST for more than sixty years , in my youth clear from the beginning, less precise than the ordination .... </span>" <br><br>

A few days later, before noon ,<span> March 22, 1832 </span>, Goethe dies, thus sealing a work that has been written, repeated several times and finished in the whole arc of a lifetime.<br><br>

<span>As the darkness of death approached, he asked his daughter-in-law, Ottile, who had looked after him after the death of Cristiane and his son Augusto, to open the shutters "to have more light".</span></p>

    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
