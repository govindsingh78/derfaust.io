<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Production</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/show1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
    <h1>Production<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marco.jpg"></span> 
      <h4>Marco Kohler</h4>
    </div>
    <div class="characters_right_box">
      <h3>Rome <abbr>(1955)</abbr></h3>
     <p>I came to the theater and to acting in 1987, almost by chance; after some experience in a parish of EUR, I started producing shows, founding a company " I GIULLARI " with which I had the opportunity to stage several shows, among which:</p>
<ul class="cinema_box">
          <li><span>L'ARMATA BRANCALEONE</span> in 1993</li>
          <li><span>THE SEVEN KING OF ROME  </span> in the same year 1993 and until 1995</li>
          <li><span>L'ODISSEA </span> in 1997</li>
          <li><span>ALLELUJA BRAVA GENTE </span>in 1999</li>
        </ul>
       <p>After this intense period of production, I went away for a few years from the scenes, for a sort of rethinking on the way to work, but in 2006, the" virus "that anyone who has trod a stage, knows how to be immortal, is back alive and has renewed the urgency to produce a work that as far back as 1989, had struck and fascinated me:<span> THE FAUST Goethe .</span> <br><br>

After a period of fervid work closely with composer Roberto<span> BROODER </span>and prolific librettist Alessandro HELLMANN, I produced this musical that I thought and I believe is of great quality, in a difficult theatrical scene and poor in ideas, like that of then and perhaps even more today, in the present moment. <br><br>

I hope that the new edition I'm working on and that should see the light between 2019 and 2020, confirm the excellent consideration that both the public and the press had reserved in 2008. <br><br>

Also in this re-release we hope that the public will continue, as in past, to perceive the passion (we all put the SOUL and could not be otherwise, given the subject) but also the great preparation and professionalism of all the artists involved. <br><br>

See you soon in the theater!</p>
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
