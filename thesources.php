<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Sources</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
     <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
     <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />

</head>

<body>
 <!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="home_banner no-pad">
  <img src="imago/le-fonti-eng.jpg">
  <div class="container banner-content">
  </div>
 </div>
<div class="slider-section"> 
  <div id="owl-demo" class="owl-carousel owl-theme"> 
    <div class="item active"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>LITERATURE</span>Faust and the Art</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Fiction</h2>
              <li>Anonymous (publisher Johann Spies), Historia von D. Iohan Fausten<br>
                (History of Dr. Faust well known magician and necromancer) (1587): 
                this is the first printed version of the story.</li>
              <li>Mikhail Bulgakov, Master and Margaret (elaboration: 1928-1940)</li>
              <li>Adelbert von Chamisso, Extraordinary story by Peter Schlemihl 
                (1814)</li>
              <li> Tom Holt, Faust Among Equals</li>
              <li> Thomas Mann, Doktor Faustus (1947)</li>
              <li>Terry Pratchett, Eric ( parody of the theme of the pact with 
                the devil)</li>
              <li>Michael Swanwick, Jack Faust (in English) </li>
              <li>Ivan Turgenev, Faust (1856) </li>
              <li>Douglass Wallop, The Year the Yankees Lost the Pennant</li>
              <li>Valery Bryusov, The Fiery Angel </li>
            </ul>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Poetry</h2>
              <li>Heinrich Heine, Der Doktor Faust</li>
              <li>Carol Ann Duffy, Mrs Faust</li>
              <li>Fernando Pessoa, Faust</li>
            </ul>
          </div>
          <!-- <div class="col-md-5 right-slide">

                            <div id="myCarousel1" class="carousel slide">
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <img src="imago/video.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video1.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video2.png" alt="video">
                                </div>
                              </div>

                             <div class="slider-control">
                                <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a>
                                <button id="toggleCarousel1"><i class="fa fa-pause"></i></button>
                                <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a>
                              </div>
                            </div>
                      </div> -->
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>THEATER</span> Faust and the Art</h1>
          </div>
          <div class="col-md-7"> 
            <ul class="cinema_box">
              <li>Christopher Marlowe, The Tragical History of Doctor Faustus 
                (1604 ~ 1610) </li>
              <li>Dorothy L. Sayers, The Devil to Pay (1939). </li>
              <li>Johann Wolfgang von Goethe, Urfaust (1772-1775) </li>
              <li>Johann Wolfgang von Goethe, Faust, Part One</li>
              <li>Johann Wolfgang von Goethe, Faust, Part Two </li>
              <li> Gotthold Ephraim Lessing, D. Faust (fragments) (1755). </li>
              <li>Gertrude Stein, Dr. Faust lights up</li>
              <li> Michel Carre, Faust and Marguerite </li>
              <li>Mark Ravenhill, Faust is Dead </li>
              <li>Paul Valéry, My Faust (Mon Faust) (1946) (posthumously, incomplete) 
              </li>
              <li>Peter Stein, FAUST (2000 ) (Complete recital in 4 DVD)</li>
            </ul>
          </div>
          <!-- <div class="col-md-5 right-slide">

                            <div id="myCarousel2" class="carousel slide">
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <img src="imago/video.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video1.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video2.png" alt="video">
                                </div>
                              </div>
                             <div class="slider-control">
                                <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a>
                                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a>
                              </div>
                            </div>
                      </div> -->
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>MUSIC</span>Faust and the Art</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2>Other musical forms</h2>
              <li>Charles-Valentin Alkan, third movement of the Great Sonatas 
                (1848)</li>
              <li>Hector Berlioz, The Damnation of Faust (1845-46)</li>
              <li>Hector Berlioz, 8 scènes de Faust</li>
              <li>Ludwig van Beethoven, Op. 75, n. 3 (1809) from Faust "Es war 
                einmal ein König"</li>
              <li>Franz Liszt, Faust-Symphonie (1854-57), Mephisto Valzer and 
                the minor sonata</li>
              <li> Gustav Mahler, Symphony n. 8 (1906-07)</li>
              <li>Modest Musorgsky: "Song of the Fleaf of Mefistofele"</li>
              <li>Alfred Schnittke, Faust Cantata (1982-83)</li>
              <li>Franz Schubert, Gretchen am Spinnrade, Lied (1814)</li>
              <li>Robert Schumann, Goethe's Scenes from Faust (completed in 1853) 
              </li>
              <li>Richard Wagner, Faust ouverture (1840) </li>
              <li>Art Zoyd's Faust song. </li>
              <li>The Doktor Faustus song by The Fall (1986), also known as Faust 
                Banana. The album La Masquerade Infernale (1997) of the Avant-garde 
                / progressive Arcturus group, alludes to the story and is dedicated 
                to the "Faustian spirit"</li>
              <li>The albums Epica (2003) and The Black Halo (2005) of the Melodic 
                power metal group Kamelot, they are inspired by Faust. </li>
              <li> The Dr. Faust album by Aton's (1994) is loosely based on Goethe's 
                Faust.</li>
              <li>The musical Foster, in the film The Phantom of the Stage by 
                B.De Palma. </li>
              <li>The song Absinthe with Faust by the group of Gothic Black Metal 
                Cradle of Filth </li>
            </ul>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Opera</h2>
              <li>Arrigo Boito, Mefistofele (1868)</li>
              <li>Havergal Brian, Faust</li>
              <li>Ferruccio Busoni, Doktor Faust (1916-25)</li>
              <li>Hervé, Le Petit Faust (operetta, 1869)</li>
              <li>Charles Gounod, Faust (1859)</li>
              <li> Sergei Prokofiev, The Fire Angel (with the characters of Faust 
                and Mefistofele)</li>
              <li>Alfred Schnittke, Historia von D. Johann Fausten</li>
              <li>Ludwig Spohr, Faust</li>
              <li>Heinrich Zoellner, Faust</li>
              <li>Igor Stravinsky, The Rake's Progress</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>CINEMA</span> Faust and The Art</h1>
          </div>
          <div class="col-md-7"> 
            <ul class="cinema_box">
              <li>Georges Méliès, Faust (1903) </li>
              <li>Henri Andréani and David Barnett, Faust (1910) </li>
              <li>Friedrich Wilhelm Murnau, <span>FAUST</span> 1926)</li>
              <li> René Clair,<span>THE BEAUTY OF THE DEVIL</span>(La Beautè 
                du Diable) (1949) </li>
              <li> Gustaf Gründgens, Faust ( 1960) </li>
              <li>Richard Burton and Nevill Coghill, "Doctor Faustus" (1967) </li>
              <li>Peter Cook and Dudley Moore, My Friend the Devil (1967), </li>
              <li>Leandro Castellani "The Fausto of Marlowe" with Tino Buazzelli 
                (1978) (TV)</li>
              <li>Jan Svankmajer, Faust (1994) </li>
              <li>Álex Ollé, Fausto 5.0 </li>
              <li>István Szabó, <span>MEPHISTO</span> with Klaus Maria Brandauer 
              </li>
              <li>Brian Yuzna,<span> FAUST </span>, Love of the Damned (2001) 
                - Genre Horror</li>
            </ul>
          </div>
          <div class="col-md-5 right-slide"> 
            <div id="myCarousel2" class="carousel slide"> 
              <div class="carousel-inner" role="listbox"> 
                <div class="item active"> <img src="imago/video.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video1.png" alt="video"> </div>
                <div class="item"> <img src="imago/video2.png" alt="video"> </div>
                <div class="item"> <img src="imago/video3.png" alt="video"> </div>
              </div>
              <div class="slider-control"> <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a> 
                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>PAINTING</span> Faust and The Art</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <li><span>Eugène Delacroix's</span> engravings on the theme of 
                Faust are famous</li>
            </ul>
          </div>
          <div class="col-md-6 right-slide"> 
            <div id="myCarousel3" class="carousel slide"> 
              <div class="carousel-inner" role="listbox"> 
                <div class="item active"> <img src="imago/video4.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video5.png" alt="video"> </div>
                <div class="item"> <img src="imago/video6.png" alt="video"> </div>
                <div class="item"> <img src="imago/video7.png" alt="video"> </div>
                <div class="item active"> <img src="imago/video8.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video9.png" alt="video"> </div>
                <div class="item"> <img src="imago/video10.png" alt="video"> </div>
                <div class="item"> <img src="imago/video11.png" alt="video"> </div>
              </div>
              <div class="slider-control"> <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a> 
                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=customNavigation> <a class="btn prev"><i class="fa fa-angle-left" aria-hidden=true></i></a> 
    <a class="btn next"><i class="fa fa-angle-right" aria-hidden=true></i></a> 
  </div>
</div>
<?php include 'footer.php';?>
    <script type="" src="js/owl.carousel.js"></script>
  <script>
      $(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel1').carousel({
    interval: 2000,
    pause: 0,
    loop: true,
  });

  /* Play trigger */
  $('#toggleCarousel1').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel1').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

  </script>
  <script>
    $(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel2').carousel({
    interval: 2000,
    pause: 0
  });

  /* Play trigger */
  $('#toggleCarousel2').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel2').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

  </script>
  <script>
    $(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel3').carousel({
    interval: 2000,
    pause: 0
  });

  /* Play trigger */
  $('#toggleCarousel3').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel3').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

  </script>
  <script type=text/javascript>
  $(document).ready(function(){var a=$("#owl-demo");
a.owlCarousel({items:1,
itemsDesktop:[1000,1],
itemsDesktopSmall:[900,1],
itemsTablet:[600,1],
itemsMobile:false});
$(".next").click(function(){a.trigger("owl.next")});$(".prev").click(function(){a.trigger("owl.prev")});$(".play").click(function(){a.trigger("owl.play",1000)});$(".stop").click(function(){a.trigger("owl.stop")})});</script>
     </body>

</html>
