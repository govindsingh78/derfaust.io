<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teatro</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
     <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
     <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />

</head>

<body>
 <!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="home_banner no-pad">
 	<img src="imago/le-fonti.jpg">
 	<div class="container banner-content">
 		<!-- <h1>Le Fonti</h1> -->
 	</div>
 </div>
<div class="slider-section"> 
  <div id="owl-demo" class="owl-carousel owl-theme"> 
    <div class="item active"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>TEATRO</span> Faust and The Art</h1>
          </div>
          <div class="col-md-7"> 
            <ul class="cinema_box">
              <li>Christopher Marlowe, The Tragical History of Doctor Faustus 
                (1604~1610) </li>
              <li>Dorothy L. Sayers, The Devil to Pay (1939).</li>
              <li>Johann Wolfgang von Goethe, Urfaust (1772-1775)</li>
              <li>Johann Wolfgang von Goethe, Faust, Parte Uno</li>
              <li>Johann Wolfgang von Goethe, Faust, Parte Due</li>
              <li>Gotthold Ephraim Lessing, D. Faust (frammenti) (1755).</li>
              <li>Gertrude Stein, Il Dottor Faust accende le luci</li>
              <li>Michel Carre, Faust et Marguerite </li>
              <li>Mark Ravenhill, Faust is Dead/li> 
              <li>Paul Valéry, Il mio Faust (Mon Faust) (1946) (postumo; incompleto)</li>
              <li> Peter Stein, FAUST (2000) (Recita integrale in 4 DVD)</li>
            </ul>
          </div>
          <!-- <div class="col-md-5 right-slide">

                            <div id="myCarousel2" class="carousel slide">
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <img src="imago/video.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video1.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video2.png" alt="video">
                                </div>
                              </div>
                             <div class="slider-control">
                                <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a>
                                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a>
                              </div>
                            </div>
                      </div> -->
        </div>
      </div>
    </div>
    <div class="item "> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>LETTERATURA </span> Faust e l'Arte</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Narrativa</h2>
              <li>Anonimo (editore Johann Spies), Historia von D. Iohan Fausten<br>
                (Storia del dottor Faust ben noto mago e negromante) (1587): questa 
                è la prima versione stampata della storia.</li>
              <li>Mikhail Bulgakov, Il maestro e Margherita (elaborazione: 1928–1940)</li>
              <li>Adelbert von Chamisso, Storia straordinaria di Peter Schlemihl 
                (1814)</li>
              <li>Tom Holt, Faust Among Equals</li>
              <li>Thomas Mann, Doktor Faustus (1947)</li>
              <li> Terry Pratchett, Eric (parodia del tema del patto con il diavolo)</li>
              <li>Michael Swanwick, Jack Faust (in inglese)</li>
              <li>Ivan Turgenev, Faust (1856)</li>
              <li>Douglass Wallop, The Year the Yankees Lost the Pennant</li>
              <li>Valery Bryusov, The Fiery Angel</li>
            </ul>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Poesia</h2>
              <li>Heinrich Heine, Der Doktor Faust</li>
              <li>Carol Ann Duffy, Mrs Faust</li>
              <li>Fernando Pessoa, Faust</li>
            </ul>
          </div>
          <!-- <div class="col-md-5 right-slide">

                            <div id="myCarousel1" class="carousel slide">
                              <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                  <img src="imago/video.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video1.png" alt="video">
                                </div>

                                <div class="item">
                                  <img src="imago/video2.png" alt="video">
                                </div>
                              </div>

                             <div class="slider-control">
                                <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a>
                                <button id="toggleCarousel1"><i class="fa fa-pause"></i></button>
                                <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a>
                              </div>
                            </div>
                      </div> -->
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>MUSICA</span> Faust and The Art</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Altre forme musicali</h2>
              <li>Charles-Valentin Alkan, terzo movimento della Grande Sonate 
                (1848)</li>
              <li>Hector Berlioz, La dannazione di Faust (1845-46)</li>
              <li>Hector Berlioz, 8 scènes de Faust</li>
              <li>Ludwig van Beethoven, Op. 75, n. 3 (1809) dal Faust "Es war 
                einmal ein König"</li>
              <li>Franz Liszt, Faust-Symphonie (1854-57), i Mephisto Valzer e 
                la sonata in si minore</li>
              <li>Gustav Mahler, Sinfonia n. 8 (1906-07)</li>
              <li>Modest Musorgskij: "Canto della pulce di Mefistofele"</li>
              <li>Alfred Schnittke, Faust Cantata (1982-83)</li>
              <li>Franz Schubert, Gretchen am Spinnrade, Lied (1814)</li>
              <li>Robert Schumann, Scene dal Faust di Goethe (completata nel 1853)</li>
              <li>Richard Wagner, Faust ouverture (1840)</li>
              <li>La canzone Faust di Art Zoyd.</li>
              <li>La canzone Doktor Faustus dei The Fall (1986), nota anche come 
                Faust Banana.</li>
              <li>L'album La Masquerade Infernale (1997) del gruppo Avant-garde/progressive 
                Arcturus, allude al racconto ed è dedicato allo "spirito Faustiano"</li>
              <li>Gli album Epica (2003) e The Black Halo (2005) del gruppo di 
                Melodic power metal Kamelot, sono ispirati al Faust.</li>
              <li> L'album Dr. Faust degli Aton's (1994) è liberamente ispirato 
                al Faust di Goethe.</li>
              <li>Il musical Foster, nel film Il fantasma del palcoscenico di 
                B.De Palma.</li>
              <li>Il brano Absinthe with Faust del gruppo di Gothic Black Metal 
                Cradle of Filth</li>
            </ul>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <h2> Opera</h2>
              <li> Arrigo Boito, Mefistofele (1868)</li>
              <li>Havergal Brian, Faust</li>
              <li>Ferruccio Busoni, Doktor Faust (1916-25)</li>
              <li> Hervé, Le Petit Faust (operetta, 1869)</li>
              <li>Charles Gounod, Faust (1859)</li>
              <li> Sergej Prokof'ev, L'angelo di fuoco (con i personaggi di Faust 
                e Mefistofele)</li>
              <li>Alfred Schnittke, Historia von D. Johann Fausten</li>
              <li>Ludwig Spohr, Faust</li>
              <li>Heinrich Zoellner, Faust</li>
              <li>Igor Stravinskij, The Rake's Progress</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>CINEMA</span> Faust and The Art</h1>
          </div>
          <div class="col-md-7"> 
            <ul class="cinema_box">
              <li>Georges Méliès, Faust (1903)</li>
              <li>Henri Andréani e David Barnett, Faust (1910)</li>
              <li>Friedrich Wilhelm Murnau, <span>FAUST</span> 1926)</li>
              <li>René Clair, <span>LA BELLEZZA DEL DIAVOLO</span> (La Beautè 
                du Diable) (1949)</li>
              <li>Gustaf Gründgens, Faust (1960)</li>
              <li>Richard Burton e Nevill Coghill, "Doctor Faustus" (1967)</li>
              <li>Peter Cook e Dudley Moore, Il mio amico il diavolo (1967),</li>
              <li>Leandro Castellani "Il Fausto di Marlowe" con Tino Buazzelli 
                (1978) (TV)</li>
              <li>Jan Svankmajer, Faust (1994)</li>
              <li>Álex Ollé, Fausto 5.0</li>
              <li>István Szabó, <span>MEPHISTO</span> con Klaus Maria Brandauer</li>
              <li>Brian Yuzna,<span> FAUST</span>, Love of the Damned (2001) - 
                Genere Horror</li>
            </ul>
          </div>
          <div class="col-md-5 right-slide"> 
            <div id="myCarousel2" class="carousel slide"> 
              <div class="carousel-inner" role="listbox"> 
                <div class="item active"> <img src="imago/video.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video1.png" alt="video"> </div>
                <div class="item"> <img src="imago/video2.png" alt="video"> </div>
                <div class="item"> <img src="imago/video3.png" alt="video"> </div>
              </div>
              <div class="slider-control"> <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a> 
                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="item"> 
      <div class="container"> 
        <div class="row"> 
          <div class="col-md-12 intro_com"> 
            <h1><span>PITTURA</span> Faust and The Art</h1>
          </div>
          <div class="col-md-6"> 
            <ul class="cinema_box">
              <li> Famose sono le incisioni di <span>Eugène Delacroix</span> sul 
                tema di Faust</li>
            </ul>
          </div>
          <div class="col-md-6 right-slide"> 
            <div id="myCarousel3" class="carousel slide"> 
              <div class="carousel-inner" role="listbox"> 
                <div class="item active"> <img src="imago/video4.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video5.png" alt="video"> </div>
                <div class="item"> <img src="imago/video6.png" alt="video"> </div>
                <div class="item"> <img src="imago/video7.png" alt="video"> </div>
                <div class="item active"> <img src="imago/video8.png" alt="video"> 
                </div>
                <div class="item"> <img src="imago/video9.png" alt="video"> </div>
                <div class="item"> <img src="imago/video10.png" alt="video"> </div>
                <div class="item"> <img src="imago/video11.png" alt="video"> </div>
              </div>
              <div class="slider-control"> <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><i class="fa fa-angle-double-left"></i></a> 
                <button id="toggleCarousel2"><i class="fa fa-pause"></i></button>
                <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><i class="fa fa-angle-double-right"></i></a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=customNavigation> <a class="btn prev"><i class="fa fa-angle-left" aria-hidden=true></i></a> 
    <a class="btn next"><i class="fa fa-angle-right" aria-hidden=true></i></a> 
  </div>
</div>
<?php include 'footer.php';?>
    <script type="" src="js/owl.carousel.js"></script>
	<script>
  		$(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel1').carousel({
    interval: 2000,
    pause: 0,
    loop: true,
  });

  /* Play trigger */
  $('#toggleCarousel1').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel1').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

	</script>
  <script>
    $(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel2').carousel({
    interval: 2000,
    pause: 0
  });

  /* Play trigger */
  $('#toggleCarousel2').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel2').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

  </script>
  <script>
    $(function() {
  /* Initialize Carousel */
  var paused = 0;
  $('#myCarousel3').carousel({
    interval: 2000,
    pause: 0
  });

  /* Play trigger */
  $('#toggleCarousel3').click(function() {
    var state = (paused) ? 'cycle' : 'pause';
    paused = (paused) ? 0 : 1;
    $('#myCarousel3').carousel(state);
    $(this).find('i').toggleClass('fa-play fa-pause');
  });
});

  </script>
  <script type=text/javascript>
  $(document).ready(function(){var a=$("#owl-demo");
a.owlCarousel({items:1,
itemsDesktop:[1000,1],
itemsDesktopSmall:[900,1],
itemsTablet:[600,1],
itemsMobile:false});
$(".next").click(function(){a.trigger("owl.next")});$(".prev").click(function(){a.trigger("owl.prev")});$(".play").click(function(){a.trigger("owl.play",1000)});$(".stop").click(function(){a.trigger("owl.stop")})});</script>
     </body>

</html>
