<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Il Cast</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/spettacolo1200x350.jpg" class="img-responsive">
 </div>

<div class="container performer-box">
    <div class="intro_com1">
      
    <h1>I PERFORMER<span></span></h1>
    </div>
    <ul class="clearfix">
      <li><a href="#" data-toggle="modal" data-target="#grafica">
        <img src="imago/pergormer1.jpg">
        <h3>MARCO</h3>
      </a></li>

      <li><a href="#" data-toggle="modal" data-target="#grafica1">
        <img src="imago/pergormer2.jpg">
        <h3>ENRICO</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica2">
        <img src="imago/pergormer3.jpg">
        <h3>VALENTINA</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica3">
        <img src="imago/pergormer4.jpg">
        <h3>CLAUDIA</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica4">
        <img src="imago/pergormer5.jpg">
        <h3>NICOLA</h3>
      </a></li>
    </ul>
    <div class="intro_com1">
      
    <h1>LE BALLERINE<span></span></h1>
    </div>
    <ul class="clearfix">
      <li><a href="#" data-toggle="modal" data-target="#grafica5">
        <img src="imago/dancers1.jpg">
        <h3>Azzurra</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica6">
        <img src="imago/dancers2.jpg">
        <h3>Silvia</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica7">
        <img src="imago/dancers3.jpg">
        <h3>Laura</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica8">
        <img src="imago/dancers4.jpg">
        <h3>Agata</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica9">
        <img src="imago/dancers5.jpg">
        <h3>Sarah</h3>
      </a></li>
    </ul>
    <ul class="clearfix last_pick">
      <li><a href="#" data-toggle="modal" data-target="#grafica10">
        <img src="imago/dancers6.jpg">
        <h3>Monica</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica11">
        <img src="imago/dancers7.jpg">
        <h3>Sara</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica12">
        <img src="imago/dancers8.jpg">
        <h3>Rossella</h3>
      </a></li>
    </ul>
</div>

<?php include 'footer.php';?>



<!-- start popup window section -->

 <!-- start one popup -->
 <div id="grafica" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Marco STABILE (Cassino)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer1.jpg"> 
          <h3>(Faust)</h3>
        </div>
        <p><span>- 2004</span> lavora nella compagnia di P.Insegno negli spettacoli 
          "<span>Insegnami a sognare</span> ", (Teatro Sistina) e " <span> Buonasera 
          Buonasera</span> " (Teatro Parioli) per la regia di C. Insegno. <br>
          <br>
          -<span>2004/05</span> fa parte del cast del musical “Joseph e la strabiliante 
          tunica dei sogni in technicolor” di A. Lloyd Webber e T. Rice con 
          Rossana Casale per la regia di C. Insegno.<br>
          <br>
          -<span> 2006</span> sempre per la regia di C.Insegno torna al Teatro 
          Sistina con lo spettacolo “<span> Un po’ prima della prima”</span> 
          ed è protagonista del musical Horror demenziale “<span>Cannibal</span> 
          ” di Trey Parker al Teatro Greco. Sempre nel 2006 fa parte del cast 
          del musical "<span> Grease </span>" della Compagnia della Rancia, nel 
          ruolo di Doody, ed è cover di Danny e Kenickie. -<span> 2007</span>debutta 
          nella prima mondiale dell'Opera Musical “<span>Il Principe della Gioventù 
          </span> ” di Riz Ortolani, nel ruolo del “Poeta Pulci”, al Teatro 
          La Fenice di Venezia. Nello stesso anno fa "<span> Sulle ali del sogno 
          </span>", per la regia di F.Draghetti e "<span>Vieni avanti cretino</span>"con 
          e di P.Insegno e R.Ciufoli, regia P.Francesco Pingitore, entrambi al 
          Salone Margherita.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end one popup -->

<!-- start two popup -->
 <div id="grafica1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enrico BERNARDI (Cuneo)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer2.jpg"> 
          <h3>MEFISTOFELE</h3>
        </div>
        <p>Si forma artisticamente e si specializza nel Musical presso la <span>Royal 
          Academy of Music di Londra e la B.S.M.T. di Bologna</span>. Ha interpretato:<br>
          <br>
          • <span>Pilato</span> in “Jesus Christ Superstar”<br>
          <br>
          • <span>Sweeney</span> in “Sweeney Todd”, corista in “Così 
          Fan Tutte” e “Falstaff”<br>
          <br>
          • <span>Napthali e Somellier</span> in “Joseph” con Rossana Casale, 
          diretto da C. Insegno<br>
          <br>
          • <span>Audrey II</span>, la voce della pianta ne “La Piccola Bottega 
          degli Orrori”, regia di F. Bellone<br>
          <br>
          • <span>Chester</span> in “Alta Società”, con Vanessa Incontrada, 
          regia M.Piparo<br>
          <br>
          • <span>Ugolino e Tommaso</span> in “La Divina Commedia" <br>
          <br>
          <span>Vocal Coach</span> di “8 Donne e un Mistero” e <span>Direttore 
          Musicale</span> di “Chiedimi se voglio la luna” con Justine Mattera, 
          regia C. Insegno.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end two popup -->

<!-- start third popup -->
 <div id="grafica2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Valentina SPALLETTA (Marino)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer3.jpg"> 
          <h3>MARGHERITA</h3>
        </div>
        <p>Ha avuto una preparazione artistica completa all'Accademia C.Pani di 
          Roma. Studia con i Maestri, A.Cerliani, C.Pani, A.M.Merli, C.Insegno, 
          P.Insegno, G. Molinari, D.Pandimiglio, oltre a frequentare diversi corsi 
          di perfezionamento vocale e di movimento del corpo riconosciuti a livello 
          nazionale.<br>
          <br>
          Debutta al Teatro Sistina con "Insegnami a sognare"; "Buonasera Buonasera" 
          al Parioli; "Cannibal il musical"; "Un po' prima della prima" al Sistina 
          tutte per la regia di C.Insegno; "Grease" compagnia della Rancia Allianz 
          theatre di Milano regia F.Bellone<br>
          <br>
          "Aspettando Barbra" di e con D.Pandimiglio; "Il principe della gioventù" 
          del M° Riz Ortolani, alla Fenice di Venezia regia M° Pier Luigi Pizzi; 
          "Sulle ali di un sogno" al Salone Margherita, regia F.Draghetti.<br>
          <br>
          In televisone approda con numerose docufiction per il programma "l'Italia 
          sul 2" e "I raccomandati" con P. Insegno.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end third popup -->

<!-- start four popup -->
 <div id="grafica3" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Claudia TORTORICI (Roma)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer4.jpg"> 
          <h3>LA STREGA, MARTA, LILITH</h3>
        </div>
        <p>Inizia la sua carriera artistica come cantante dall’incontro con 
          Stefano Zanchetti, un vocal motivational coach, che dal 1992 è il suo 
          maestro di canto.<br>
          <br>
          Partecipa a trasmissioni televisive su reti regionali (Lo scrigno dei 
          desideri a Teleambiente "Il processo dei tifosi" a Teleroma 56 e "I 
          raccomandati" a Rai 1)<br>
          <br>
          Nel <span>2005</span> è co-protagonista con il ruolo di <span>SAMIR</span> 
          nel musical "<span>Il miracolo di Padre Pio</span>" per la regia di 
          Daniela Pistagna dove mette in mostra anche le sue doti di attrice affinate 
          con Francesca Viscardi Leonetti assistente di Susan Batson Actor’s 
          Studio NYC.<br>
          <br>
          - <span>2006</span> Sotto la sua direzione mette in scena "<span>Anna 
          Cappelli</span>" di Annibale Ruccello.<br>
          <br>
          Quest'anno ha appena finito di girare il film indipendente <span>GIOVANI 
          DISPONIBILI</span> del regista Riccardo Camilli nel ruolo di <span>Betta</span>. 
        </p>
      </div>
    </div>
  </div>
</div>
 <!-- end four popup -->

 <!-- start five popup -->
 <div id="grafica4" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nicola ZAMPERETTI (Valdagno)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer5.jpg"> 
          <h3>WAGNER, VALENTINO </h3>
        </div>
        <p>Muove i suoi primi passi di danza a 15 anni nella scuola ARDOR di Cornedo 
          Vicentino e in seguito studia anche classico con Antonella Disconzi.<br>
          <br>
          Nel 1997 dopo il suo primo provino come ballerino per il programma di 
          RAIUNO “Faccia Tosta”, si trasferisce a Roma dove studia presso 
          lo <span>IALS</span> con Sara Greco, Mauro Astolfi, Roberto Pallara 
          e Marco Garofalo.<br>
          <br>
          - 1999 E' attore-ballerino nel Colossal “<span>U-571</span>” per 
          la regia di Jonathan Moscow, e al film per la tv “<span>Piccolo mondo 
          antico</span>”, regia Cinzia Torrini.<br>
          Negli anni completa la sua preparazione come Performer studiando recitazione 
          con Lino Damiani.<br>
          <br>
          - 2006 conduce “<span>Voilà</span>” su Sky Leonardo<br>
          <br>
          - 2007 consegue una borsa di studio per il corso di “metodo Strasberg” 
          presso la Konstanlee diretta da Ilza Prestinari.<br>
          <br>
          - 2008 è nelle Fiction “<span>Provaci ancora Prof 3</span>” e “<span>Il 
          Sangue e la rosa</span>”.<br>
          <br>
          Lavora in numerose trasmissioni televisive, tra cui: “<span>Sarabanda</span>”, 
          “<span>Carramba che fortuna</span>”, “<span>Va dove ti porta il 
          cuore</span>”, “<span>Domenica IN</span>”,<br>
          <br>
          Inoltre è performer in vari <span>MUSICAL</span>: “Grease”, “Il 
          ritratto di Dorian Gray”, “Giovanna D’arco”, “Candido”, 
          “Il principe della gioventù”, “Chorus line”, “Beggar’s 
          opera”.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end five popup -->

 <!-- start six popup -->
 <div id="grafica5" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Azzurra CACCETTA</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers1.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>Studia danza classica, contemporanea e teatro-danza a Lecce con i maestri 
          <span>Ioan Iosif Girba</span> (primo ballerino dell' opera di Budapest), 
          Enza Curto, Michele Abbondanza.<br>
          <br>
          Trasferitasi a Roma approfondisce gli stili Modern-Contemporaneo e Hip 
          Hop con i maestri <span>Astolfi e Silgoner</span>; inoltre fa anche 
          esperienza come attrice-cantante studiando con i maestri Claudio Boccaccini, 
          Gianluca Ferrato, Rossana Casale, Raffaella Misiti.<br>
          <br>
          Prende parte come ballerina-attrice-cantante in diversi musical, video-clip, 
          programmi tv, film. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end six popup -->

 <!-- start Seven popup -->
 <div id="grafica6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Silvia FLORIDI</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers2.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>In Televisione partecipa a:<br>
          <br>
          "Numero Uno", "<span>Furore</span>", "Festa della mamma", "In bocca 
          al lupo", "Per tutta la vita", oltre a spot pubblicitari e parti cinematografiche 
          (<span>APRILE e PINOCCHIO</span>).<br>
          <br>
          Tra le esperienze teatrali più significative ricordiamo:<br>
          <br>
          “<span>Un paio d'ali</span>”,<br>
          “<span>Stanno suonando la nostra canzone</span>”<br>
          ”<span>Promesse, promesse</span>”,<br>
          ”<span>La mia favola infinita</span>”<br>
          “<span>Se stasera sono qui</span>”</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Seven popup -->

 <!-- start Eight popup -->
 <div id="grafica7" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Laura Di BIAGIO</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers3.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>Diplomata in danza classica, contemporanea e jazz presso il Centro 
          Studi Danza “Tersicore”, continua a studiare in Italia e all’estero; 
          si dedica anche alle discipline del musical diplomandosi presso la “<span>Musical 
          Theatre Academy</span>”.<br>
          <br>
          Professionalmente è impegnata nel cinema e in televisione, ma la sua 
          passione è il teatro. Si esibisce come ballerina, attrice, cantante 
          in Italia e all’estero, con varie compagnie di danza, teatro e musical.<br>
          <br>
          Attualmente è impegnata al Sistina per il musical "<span>I Sette Re 
          di Roma</span>" e con le compagnie “I danzatori scalzi”, “<span>Open 
          Dance Theatre</span>” e “Incontempo” per la danza.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Eight popup -->

 <!-- start Nine popup -->
 <div id="grafica8" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agata MOSCHINI</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers4.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>Studia danza classica da 3 anni di età, con maestri quali Sabino Rivas 
          e Victor Litinov. E' insegnante presso il C.S.D. di A. Paganini e partecipa 
          a vari spettacoli televisivi, tra i quali "Telethon", "Uno Mattina","<span>Sognando 
          Hollywood</span>" con Matilde Brandi e "Vita da paparazzi" con P.Pingitore. 
          Si avvicina al teatro con Gianni Salvo al "Piccolo Teatro" di Catania 
          e partecipa a "<span>Distretto di Polizia 5</span>". Ha fatto la ballerina-attrice 
          al Teatro Olimpico ne "<span>La vedova allegra</span>" e al Salone Margherita 
          in "Al cavallino bianco" e "<span>Cincillà</span>" e "<span>Forza venite 
          gente!</span>" con M.Paulicelli. Al cinema ha partecipato ai film "Tutta 
          la vita davanti" di P.Virzì e "No problem" con V.Salemme. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end nine popup -->

 <!-- start Ten popup -->
 <div id="grafica9" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sarah POLITO</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers5.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>Studia danza classica da 3 anni di età, con maestri quali Sabino Rivas 
          e Victor Litinov. E' insegnante presso il C.S.D. di A. Paganini e partecipa 
          a vari spettacoli televisivi, tra i quali "Telethon", "Uno Mattina","<span>Sognando 
          Hollywood</span>" con Matilde Brandi e "Vita da paparazzi" con P.Pingitore. 
          Si avvicina al teatro con Gianni Salvo al "Piccolo Teatro" di Catania 
          e partecipa a "<span>Distretto di Polizia 5</span>". Ha fatto la ballerina-attrice 
          al Teatro Olimpico ne "<span>La vedova allegra</span>" e al Salone Margherita 
          in "Al cavallino bianco" e "<span>Cincillà</span>" e "<span>Forza venite 
          gente!</span>" con M.Paulicelli. Al cinema ha partecipato ai film "Tutta 
          la vita davanti" di P.Virzì e "No problem" con V.Salemme. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Ten popup -->

 <!-- start Elaven popup -->
 <div id="grafica10" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Monica RAMIRES</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers6.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p> Ballerina, attrice, cantante ha partecipato a spettacoli teatrali, 
          tra i quali, ultimamente, “<span>Epidemia</span>” e “<span>Anja</span>” 
          diretti da Claudio Boccaccini, “Pericle principe di Tiro” diretto 
          da Riccardo Reim, e i musical “<span>Polvere quando il pensiero balla</span>” 
          diretto da R. Capitani, “<span>Alice nel mondo della danza</span>” 
          diretto da Silvia Perelli.<br>
          <br>
          Ballerina in programmi televisivi RAI e Mediaset: “<span>Un disco 
          per l’estate</span>”, “La sai l’ultima”, “Per tutta la vita”, 
          “Seven Show”, “Girofestival” , “<span>Sarabanda</span>”.<br>
          <br>
          Ballerina di videoclips. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Elaven popup -->

 <!-- start Twel popup -->
 <div id="grafica11" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sara TELCH</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers7.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p>Inizia i suoi studi di danza all’età di cinque anni, diplomandosi 
          nel 2002 al Balletto di Roma.<br>
          <br>
          Lavora nel musical “<span>Rodolfo Valentino</span>”, “<span>Il 
          Fantasma dell’Opera</span>”, “Profondo Rosso”, e nel musical 
          “<span>Fame</span>”nel ruolo di Carmen Diaz.<br>
          <br>
          Partecipa come ballerina al programma televisivo “<span>Amici</span>” 
          di Maria De Filippi.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Twel popup -->

 <!-- start Thirteen popup -->
 <div id="grafica12" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rossella PUGLIESE</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers8.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p> Diplomata nel 2004 in danza classica, moderna e contemporanea presso 
          la ''<span>British Art of London</span>'.<br>
          <br>
          Nel 2005 diplomata presso<span> L' AID (Associazione Italiana Danzatori)</span> 
          in jazz, hip hop.<br>
          <br>
          Nel 2008 si è diplomata presso l'accademia teatrale 'La Stazione' di 
          Claudio Boccaccini.<br>
          <br>
          Tra i suoi ultimi lavori: <span>Arbaith; No smoking</span>; Arrivederci 
          forse mai;<br>
          <br>
          <span>Nostos</span> di C.Boccaccini;<br>
          <br>
          <span>Mamma Napoli</span> di E.De Martino </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Thirteen popup -->









 <!-- end popup window section -->

</body>
</html>
