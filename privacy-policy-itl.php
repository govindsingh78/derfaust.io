<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privacy Policy</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                      <input type="text" class="form-control" placeholder="Search">
                      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                  </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
            <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container the_plot">
<div class="intro_com1">
  <h1>Politica dei cookie<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>Il Garante per la Protezione dei Dati Personali ha emanato lo scorso 8 Maggio 2014 il provvedimento n. 229, dal titolo "Individuazione delle modalità semplificate per l'informativa e l'acquisizione del consenso per l'uso dei cookie", con conseguente pubblicazione sulla Gazzetta Ufficiale in data 3 Giugno 2014. Attraverso tale provvedimento il Garante recepisce le direttive europee secondo le quali ciascun sito web, al primo accesso di un visitatore, deve mostrare un banner informativo che li informi della politica dei cookie operata dal sito che gli stessi stanno visitando, e di subordinare il proseguimento della navigazione all'accettazione di tale politica.<br>
    La presente cookie policy è stata aggiornata il <span class="rd_clr">24 maggio 2018</span>. Eventuali aggiornamenti saranno sempre pubblicati in questa pagina.</p>
    </div>
  </div></div>
  <div class="container the_plot">
<div class="intro_com1">
  <h1>Definizione<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>I cookie sono stringhe di testo di piccole dimensioni che i siti visitati dall'utente inviano al suo terminale (solitamente al browser), dove vengono memorizzati per essere poi ritrasmessi agli stessi siti alla successiva visita del medesimo utente. Nel corso della navigazione su un sito, l'utente può ricevere sul suo terminale anche cookie che vengono inviati da siti o da web server diversi (c.d. "terze parti"), sui quali possono risiedere alcuni elementi (quali, ad esempio, immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando.
        <br>
I cookie, solitamente presenti nei browser degli utenti in numero molto elevato e a volte anche con caratteristiche di ampia persistenza temporale, sono usati per differenti finalità: esecuzione di autenticazioni informatiche, monitoraggio di sessioni, memorizzazione di informazioni su specifiche configurazioni riguardanti gli utenti che accedono al server, ecc.<br><br>

Il Garante, al fine di giungere ad una corretta regolamentazione della direttiva, ha individuato due macro categorie di cookie: cookie tecnici e cookie di profilazione; e ha contestualmente definito una ulteriore differenziazione in merito al soggetto che installa i cookie, che può essere lo stesso gestore del sito ("editore") o un soggetto terzo, che installa i cookie per il tramite del primo, definendo questa tipologia cookie di terze parti. In quest'ultimo caso il Garante ha espressamente limitato la responsabilità dell'editore al rimando all'informativa della parte che installa tali cookie.</p>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Cookie tecnici<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>I cookie tecnici sono quelli utilizzati al solo fine di "effettuare la trasmissione di una comunicazione su una rete di comunicazione elettronica, o nella misura strettamente necessaria al fornitore di un servizio della società dell'informazione esplicitamente richiesto dall'abbonato o dall'utente a erogare tale servizio" (cfr. art. 122, comma 1, del Codice).<br><br>

Essi non vengono utilizzati per scopi ulteriori e sono normalmente installati direttamente dal titolare o gestore del sito web. Possono essere suddivisi in cookie di navigazione o di sessione, che garantiscono la normale navigazione e fruizione del sito web (permettendo, ad esempio, di realizzare un acquisto o autenticarsi per accedere ad aree riservate); cookie analytics, assimilati ai cookie tecnici laddove utilizzati direttamente dal gestore del sito per raccogliere informazioni, in forma aggregata, sul numero degli utenti e su come questi visitano il sito stesso; cookie di funzionalità, che permettono all'utente la navigazione in funzione di una serie di criteri selezionati (ad esempio, la lingua, i prodotti selezionati per l'acquisto) al fine di migliorare il servizio reso allo stesso.</p><br>
<p>Per l'installazione di tali cookie non è richiesto il preventivo consenso degli utenti, mentre resta fermo l'obbligo di dare l'informativa ai sensi dell'art. 13 del Codice.</p><br>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Cookie di profilazione<span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>I cookie di profilazione sono volti a creare profili relativi all'utente e vengono utilizzati al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dallo stesso nell'ambito della navigazione in rete. In ragione della particolare invasività che tali dispositivi possono avere nell'ambito della sfera privata degli utenti, la normativa europea e italiana prevede che l'utente debba essere adeguatamente informato sull'uso degli stessi ed esprimere così il proprio valido consenso.<br><br>

Ad essi si riferisce l'art. 122 del Codice laddove prevede che "l'archiviazione delle informazioni nell'apparecchio terminale di un contraente o di un utente o l'accesso a informazioni già archiviate sono consentiti unicamente a condizione che il contraente o l'utente abbia espresso il proprio consenso dopo essere stato informato con le modalità semplificate di cui all'articolo 13, comma 3" (art. 122, comma 1, del Codice).<br><br>

Il nostro sito, facendo riferimento a quanto sopra descritto, installa sul terminale dell'utente solo ed esclusivamente cookie tecnici di prima e di terze parti tra cui:<br><br>

Google Analytics - Sistema di statistiche<span> leggi l'informativa completa  </span>(modificato in modo tale da anonimizzare la visita, così da rendere impossibile la profilazione dell'utente)<br>
Facebook - Social Media<span> leggi l'informativa completa</span><br>
YouTube - Contenuti video<span> leggi l'informativa completa</span></p>

<h5><span>Nessun dato personale degli utenti viene in proposito acquisito dal sito</span></h5>
<h5>Controllare le impostazione relative ai cookie e disattivarli</h5>
<p>I cookie facilitano e velocizzano la navigazione sul nostro sito, puoi comunque scegliere, se lo desideri, di rifiutarli. Ciò, però, ci impedirà di ricordare i tuoi preferiti, di offrirti contenuti personalizzati e di raccogliere dati aggregati ricavati dalla tua visita sul sito. Puoi modificare le impostazioni relative ai cookie direttamente dal tuo browser web.<br><br>

Per saperne di più su come gestire ed eliminare i cookie dal tuo browser, vai ai seguenti collegamenti</p>
<ul class="cinema_box">
          <li>Ulteriori informazioni sulla disabilitazione dei cookie su<span> Firefox</span></li>
          <li>Ulteriori informazioni sulla disabilitazione dei cookie su<span> Chrome</span></li>
          <li>Ulteriori informazioni sulla disabilitazione dei cookie su<span>Safari</span></li>
          <li>Ulteriori informazioni sulla disabilitazione dei cookie su<span> Internet Explorer</span></li>
        </ul>
        <p>Per tuo uso personale puoi in ogni momento utilizzare il sito <span>www.youronlinechoices.com </span>per configurare l'utiizzo dei cookie pubblicitari di terzi parti.</p><br>
    </div>
  </div>
</div>
<div class="container the_plot">
<div class="intro_com1">
  <h1>Diritti dell'interessato <span></span></h1></div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
        <p> Ai sensi ai sensi dell'art. 15 del GDPR (Diritto di accesso ai dati personali ed altri diritti) del Codice della Privacy, i vostri diritti in ordine al trattamento dei dati sono:</p>
<ul class="cinema_box">
          <li>conoscere, mediante accesso gratuito l'esistenza di trattamenti di dati che possano riguardarvi; </li>
          <li>essere informati sulla natura e sulle finalità del trattamento;</li>
          <li>Ottenere a cura del titolare, senza ritardo:</li>
          <li>informazioni circa l’esistenza dei propri dati personali, l’origine degli stessi, le finalità e le modalità di trattamento e, qualora presenti, di ottenere l’accesso ai datipersonali ed alle informazioni di cui all’articolo 15 del GDPR.</li>
          <li>richiedere l’aggiornamento, la rettifica, l’integrazione, la cancellazione, la limitazione del trattamento dei dati nel caso ricorra una delle condizioni previste all’articolo           18 del GDPR, la trasformazione in forma anonima o il blocco dei dati personali, trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione           in relazione agli scopi per i quali i dati sono stati raccolti e/o successivamente trattati.

</li>
<li>opporsi, in tutto o in parte, per motivi legittimi, al trattamento dei dati, ancorché pertinenti allo scopo della raccolta ed al trattamento dei dati personali previsti ai fini          di informazione commerciale o di invio di materiale pubblicitario o di vendita diretta ovvero per il compimento di ricerche di mercato o di comunicazione commerciale.
          Ogni utente ha altresì il diritto di revocare il consenso in qualsiasi momento senza pregiudicare le liceità del trattamento basata sul consenso prestato prima della           revoca.</li>

     <li>ricevere i propri dati personali, forniti consapevolmente ed attivamente o attraverso la fruizione del servizio, in un formato strutturato, di uso comune e leggibile da           dispositivo automatico, e di trasmetterli ad un altro titolare del trattamento senza impedimenti.</li>

     <li>proporre reclamo presso l’Autorità Garante per la protezione dei dati personali in Italia</li>

     <li>propose a complaint with the Italian Data Protection Authority.</li>
        </ul>
        <p>Il titolare del trattamento dei dati ad ogni effetto di legge è:</p>
        <p><span>Sig. Marco Kohler </span></p>
        <p>E-mail: <span>info@derfaust.it</span></p>
        <h5>Cliccando su <span>PROSEGUI</span>, acconsenti all'utilizzo dei cookie sopra descritti e alla scrittura di un cookie tecnico (con validità 12 mesi) per memorizzare questa tua scelta e chiudere questa finestra.</h5>
    </div>
  </div>
</div>
<?php include 'footer.php';?>
</body>

</html>
