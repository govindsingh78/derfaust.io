<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Goethe</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/la_opera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container the_plot">
<div class="intro_com1">
    <h1>Goethe<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/goethe.jpg"></span> 
    </div>
    <div class="characters_right_box">
      <h3>J.W.Goethe <abbr>(1749 - 1832)</abbr></h3>

     <p><span>Johann Wolfgang von Goethe</span>, poeta, drammaturgo, romanziere e scienziato tedesco, nacque a Francoforte nel 1749 , figlio di un funzionario dell'amministrazione imperiale; a 16 anni lasciò Francoforte per studiare legge a Lipsia e poi a Strasburgo.<br>
Furono anni di intensa vita sociale e culturale; si interessò alla medicina, alle arti figurative, al disegno, e cominciò a scrivere versi libertini e scherzosi (Il libro di Annette). Dello stesso periodo è una tragedia in versi, I Complici (1768), cui seguono i drammi Goetz von Berlichingen (1771), Clavigo e Stella.<br>
Alla rottura del breve idillio con Käthchen Schömkopf seguì una fase di turbamento e agitazione: poi nel 1768, ammalatosi gravemente, fece ritorno a Francoforte e, superata la fase critica della malattia, durante la convalescenza si dedicò a studi di magia, astrologia, alchimia.<br><br>

Dal 1770 al 1771 visse a Strasburgo, dove accanto alle discipline giuridiche, coltivò lo studio della musica, dell'arte, dell'anatomia e della chimica; qui ebbe la rivelazione dell'arte gotica tramite Herder, e si innamorò di <span>  Friederike Brion </span>, figlia del pastore protestante di Sesenheim. La gioia e le tensioni di quell'amore gli ispirarono alcune delle più belle liriche di questo periodo, mentre il sentimento di colpa seguito all'abbandono di Friederike diventerà, trasposto, quello di Faust verso Margherita.
<br><br>


Tra il maggio e il settembre 1771 Goethe era stato a Wetzlar come praticante presso il tribunale. Là si era innamorato di <span> Charlotte Buff </span>; e di ritorno a Francoforte, traspose quell'amore irrealizzabile nel romanzo epistolare I dolori del giovane Werther, che suscitò grande scandalo ma conquistò a Goethe un successo internazionale.
<br><br>

Nel 1775 si trasferì alla corte di Weimar, ove divenne consigliere di stato. L'amore per <span> Charlotte von Stein </span> e il successo internazionale delle sue opere (in particolare lo scandalo suscitato dal Werther), fecero di Goethe il dominatore incontrastato della scena letteraria tedesca. Nel 1775 Goethe viaggiò in Svizzera insieme ai fratelli Stolberg e si spinse fino al Gottardo, attirato dall'Italia. Tornato a Francoforte, ruppe il fidanzamento con la Schönemann.
<br><br>

A Strasburgo ebbe due incontri che sarebbero stati molto importanti nella sua vita e determinanti per la sua opera letteraria. Il primo fu quello con Friederike Brion, figlia di un pastore protestante che Goethe amò e che avrebbe fornito il modello per vari suoi personaggi femminili. Il secondo fu l'incontro con il filosofo e critico letterario Johann Gottfried von Herder con cui strinse amicizia: Herder, fra l'altro, lo portò a sottrarsi all'influenza del classicismo francese, e lo introdusse all'opera di Shakespeare, in cui proprio il mancato rispetto delle tradizionali unità contribuisce all'intensità drammatica.
<br><br>
Herder, inoltre, indusse Goethe ad approfondire il significato della poesia popolare tedesca e delle forme dell'architettura gotica quali fonti d'ispirazione letteraria.
<br><br>
L'opera, che prende a modello Shakespeare, ha come protagonista un cavaliere del Cinquecento in rivolta contro l'autorità dell'imperatore e della chiesa, e anticipa i fremiti libertari che sarebbero stati l'anima del movimento Sturm und Drang, antesignano del romanticismo tedesco.
<br><br>
Al ritorno a Weimar (1788) Goethe trovò un'atmosfera ostile nei circoli letterari, mentre a corte mai si accettava la sua relazione con Christiane Vulpius, una giovane che nel 1789 gli diede un figlio e che egli avrebbe sposato nel 1806.
<br><br>
Malgrado tutto rimase a Weimar, trattenuto da due motivi d'interesse: la direzione del teatro ducale, che tenne dal 1791 al 1813, e la possibilità di perseguire meglio che altrove gli studi scientifici, cui si dedicò con rinnovato ardore. Risalgono a questi anni vari scritti di anatomia comparata (1784), di botanica (1790) e due volumi di ottica (1791 e 1792).
<br><br>
<span> Fu l'amicizia con Friedrich von Schiller </span> a riavvicinare Goethe alla letteratura e dalla loro collaborazione, durata dal 1794 alla morte di Schiller nel 1805, scaturirono numerose composizioni liriche ed epiche, l'idilio in esametri Arminio e Dorotea (1797), il dramma La figlia naturale (1802), la seconda versione del romanzo Gli anni di noviziato di Wilhelm Meister (1796), che avrebbe costituito un modello narrativo per la successiva produzione letteraria tedesca, inaugurando il genere del romanzo di formazione e soprattutto, su incoraggiamento di Schiller, la versione definitiva del Faust (la prima parte era stata pubblicata nel 1808).
<br><br>
<span> Dal 1805 fino alla morte Goethe visse anni di intensa creatività </span>. I grandi avvenimenti storici della sua epoca trovarono in lui un osservatore attento ma non appassionato: la rivoluzione francese fu da lui considerata con un certo sospetto, non tanto come espressione di un'istanza di libertà, quanto come lo scoppio incontrollato di forze oscure e disordinate; egli ammirò Napoleone, mentre vide con sospetto la prospettiva dell'unificazione della Germania.
<br><br>

Fra gli scritti di questi anni vanno ricordati il romanzo <span> Le affinità elettive (1809),</span> l'autobiografia, la raccolta di liriche Poesia e verità, <span> Il divano occidentale-orientale (1819), </span> dai toni mistici ed erotici, licenziosi e ambigui; <span> la seconda parte del FAUST (pubblicata postuma nel 1832) </span>

Il 17 Marzo scrive a Wilhelm von Humboldt: “<span> Sono più di sessant’anni che avevo in me la concezione del FAUST, in gioventù chiara fin dal principio, meno precisa quanto all’ordinamento.... </span>”
<br><br>
Pochi giorni dopo, prima di mezzogiorno, il <span> 22 Marzo 1832 </span>, Goethe muore, sigillando così un’opera che è stata scritta, ripresa più volte e terminata nell’arco intero di una vita.
<br><br>
<span>
All'avvicinarsi dell'oscurità della morte, chiese alla nuora Ottile, che lo aveva accudito dopo la morte di Cristiane e del figlio Augusto, che venissero aperte le persiane "per avere più luce".</span>
</p>

    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
