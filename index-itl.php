<?php
session_start();
include 'config.php';

$actual_link = "https://" . $_SERVER[HTTP_HOST] . "" . $_SERVER[REQUEST_URI];

if ($language == 'en') {

    if ($actual_link != "https://workdemo4u.com/musikohl/index.php?lang=en") {
        header('Location:  index.php?lang=en');
        exit;
    }

} else if ($language == 'it') {

    if ($actual_link != "https://workdemo4u.com/musikohl/index-itl.php?lang=it") {
        header('Location:  index-itl.php?lang=it');
        exit;
    }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">



                <?php include 'navbar.php';?>


    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/home.jpg" class="img-responsive">
 </div>
 <div class="container content_box">
<div class="intro_com">
 	<div class="row">
	 	
      <h1><span>Introduzione</span> IL Desiderio che Travolge L'Anima !</h1>
	 	<div class="col-md-6 no-pad">
			<p>
            Questo Musical è nato dall'idea di portare in scena un capolavoro del romanticismo Tedesco; come lo stesso Goethe ebbe a dire, quest'opera è" <span class="rd_clr">incommensurabile</span> "  ed è frutto di un lavoro che lo scrittore tedesco sviluppò durante l'intero arco della sua esistenza, apportando l'ultima revisione, solo pochi mesi prima della morte.</p>

            <p>Concepito e realizzato attraverso alcuni anni di lavoro appassionato e appassionante, con l'aiuto di 2 artisti che hanno saputo rendere al meglio le mie idee realizzative: il sensibile compositore <span class="rd_clr">M°Roberto CHIOCCIA</span> ed il raffinato ed eclettico librettista<span class="rd_clr">Alessandro HELLMANN.</span><br>
            A loro va il mio ringraziamento per aver lavorato così bene e in un modo così ispirato, da consentirmi il "rischio" di questa impresa, frutto sopratutto di grande amore per il teatro in generale e per il musical in particolare.<br>

            E' una<span class="rd_clr">grande FAVOLA POPOLARE</span>seria ma non seriosa! In questo senso è<span class="rd_clr">godibile da spettatori di ogni età e cultura</span> con musiche pop, venate e influenzate dalla classica opera italiana;<br>

            Le<span class="rd_clr">COREOGRAFIE,</span>belle e coinvolgenti, sono state realizzate da.<span class="rd_clr">Stefano BONTEMPI.</span><br>

            Il<span class="rd_clr">CAST </span>è il risultato di un lungo e meticoloso lavoro di selezione che ha prodotto una compagnia in cui agiscono giovani talenti.</p>
		</div>
        <section class="har_section har_image_bck" data-color="#f2f2f2" id="featured">
    		<div class="col-md-6 no-pad">
                <div class="har_team_slider">
        			<div class="har_shop_al_item_bl">
        	            <a href="#" class="har_shop_al_item">
        	                <span class="har_shop_item_disk">
        	                    <span class="har_shop_item_cover har_image_bck" data-image="images/video.png" class="img-responsive" style="background-image: url(&quot;images/video.png&quot;);" class="img-responsive"></span>
        	                    <span class="har_shop_item_vinyl">
        	                        <span class="har_shop_item_vinyl_img har_image_bck" data-image="images/video.png" class="img-responsive" style="background-image: url(&quot;images/video.png&quot;);" class="img-responsive"></span>
        	                        <span class="har_shop_item_vinyl_hole"></span>
        	                    </span>
        	                </span>
        	            </a>
                	</div>
                </div>
        	</div>
        </section>
	</div>
</div>
 <div class="intro_com1">
 	<div class="row">
	 	
      <h1>La Filosofia
        <!-- <span><button class="view_more pull-right">View More</button></span> -->
      </h1>
	 	<div class="col-md-11 no-pad">
	 			<p>La storia ha come protagonista uno studioso, Johann Faust, che, ormai vecchio, non avendo trovato risposte al senso della vita attraverso i suoi studi, viene tentato dal demonio Mefistofele e scommette la propria anima in cambio di giovinezza, sapienza e potere. <br>
Ora Faust, onnipotente, può disporre delle sorti altrui: si innamora della bella popolana Margherita e riesce (con l'aiutino di Mefistofele) a conquistarla; Faust la induce a somministrare un sonnifero (dato da Mefistofele) alla propria madre per potersi incontrare.<br>
Il sonnifero diabolico, però, è in realtà un potente veleno, che uccide la madre.<br>
Uscendo dalla casa di Margherita, Faust si scontra in duello con Valentino, che difende l'onore della sorella; Faust riesce ad ucciderlo, aiutato dalle arti demoniache di Mefistofele e scappa con lui per evitare la cattura.<br>
Margherita, viene maledetta dal fratello morente; abbandonata da Faust e disperata perchè è incinta, dopo il parto uccide il neonato (affogandolo). Per questo viene giudicata, condannata a morte e incarcerata.<br>
In the meantime Faust lives new adventures with Mephistopheles: he is led to a Sabbath (the council of witches and demonic powers), but during his performance appears as a vision, the figure of Margaret in prison.
Faust and Mephistopheles then go to jail to free her, but Margaret refuses her freedom, feeling that her salvation is the work of a demon and chooses to face torture. <br>
Faust then tears his bet with Mephistopheles, who makes him return old.<br>
Desperate runs to the gallows of Margaret, to be forgiven and despite being back old, is recognized by Margaret with the eyes of love (which never grow old) and gets on the gallows, agreeing to burn with her. <br>
Even the Omnipotent recognizes the desire for good and for knowledge which was at the origin of so much sinning and redeeming them.<br>
The work, allegory of human life in the whole range of passions, miseries and moments of greatness, affirms the right and the ability of the individual to want to know the divine and the human,"<span class="rd_clr">la capacità dell'uomo di essere misura di tutte le cose ".</span> </p>
<h3>Dedicato con affetto, alla memoria dei miei cari genitori Adelmo e Adele Marco KOHLER</h3>
	 	</div>
	 </div>
</div>
</div>
<!-- Modal Starts-->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false" style="top: 20%">
    
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <h4 class="modal-title">Privacy Policy</h4>
      </div>
      <div class="modal-body" style="padding: 10px;"> 
        <p>Questo sito utilizza i cookie per migliorare i servizi e l'esperienza 
          dei lettori. Se decidi di continuare la navigazione consideriamo che 
          accetti il ​​loro uso. </p>
      </div>
      <div class="modal-footer"> <a   id="permit" onclick="func1()" class="btn btn-primary">OK</a> 
        <a href="privacy-policy-itl.php?lang=it" target="_blank" class="btn btn-info">Ulteriori 
        informazioni</a> </div>
    </div>
  </div>
  </div>
<!-- Modal Ends-->
 <?php include 'footer.php';?>
<script type="text/javascript">

    function func1(){
         localStorage.setItem('popState','musikohl1');
         console.log("Clicked Me Already !!");
          $("#myModal").modal('hide');

     }

    $(document).ready(function(){

        if(localStorage.getItem('popState') == 'musikohl1'){
            $("#myModal").modal("hide");

        }

        if(localStorage.getItem('popState') !== 'musikohl1'){
          $("#myModal").modal("show");

        }

    });
</script>
</body>

</html>
