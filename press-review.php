<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Press Review</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
    <link href="css/style.css" rel="stylesheet">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
	<div class="cus-nav">
	    <nav class="navbar navbar-default bootsnav top_nav">

	        <!-- Start Top Search -->
	        <div class="top-search">
	            <div class="container">
	                
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
	            </div>
	        </div>
	        <!-- End Top Search -->

	        <div class="container-fluid top_hdr">
	            <?php include 'navbar.php';?>

    <div class="clearfix"></div>
     <div id="grafica" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">La Repubblic.it</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/la_repubblica.gif"> 
        <h4 class="modal-title"> Domenica 7 Dicembre 2008 </h4>
        <img src="imago/repubblic.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>

    <div id="corsera" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Corriere Della Sera</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/corsera.gif"> 
        <h4 class="modal-title">Mercoledì 26 Novembre 2008</h4>
        <img src="imago/corsera.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>

    <div id="giornale" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">il Giornale</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/giornale.gif" class="img-responsive"> 
        <h4>Mercoledì 17 Dicembre 2008 </h4>
        <h6> Il Musical «Faust» senza lieto fine ma tutto da cantare</h6>
        <p>di Paolo Scotti.</p>
        <p>L’attenzione crescente che si sta sviluppando attorno al musical, comincia 
          a offrire sviluppi inattesi. Un genere che per antonomasia richiede 
          grandi spazi e mezzi generosi, oggi approda sui palchi ridotti dei teatri 
          «off». Senza per questo perdere in efficacia espressiva. È il caso del 
          Faust di Goethe ridotto a musical «da camera» per le raccolte misure 
          del teatro Sala Uno (in scena fino al 21 dicembre) a opera di un valoroso 
          manipolo d'interpreti. Scelta coraggiosa, in gran parte premiata dai 
          risultati. La riduzione di Marco Kohler e il libretto di Alessandro 
          Hellmann, infatti, semplificano il colosso originale alle sue linee 
          essenziali (forse anche troppo essenziali: perché eliminare, a esempio, 
          la salvezza finale del protagonista?) mediando tra i dilemmi esistenziali 
          di Faust e la sua storia d’amore con l’incolpevole Margherita. La regia 
          di Claudio Boccaccini, inoltre, movimenta una trama in sé più filosofica 
          che dinamica, e grazie a un accorto uso di moduli scenici (scale e pedane 
          in continuo movimento), sopperisce all’inevitabile nudità dello spazio. 
          Ma un musical, si sa, ha la sua ragion d’essere soprattutto nella musica. 
          Lo spartito composto da Roberto Chioccia deriva dalla struttura del 
          musical anglosassone: cantato dall’inizio alla fine, impegna gli interpreti 
          in una sorta di «declamato» continuo. Ne consegue che quanto più si 
          distacca dallo schema «melodrammatico», offrendo brani più personali 
          e cantabili, tanto più la musica di Chioccia coglie nel segno, risultando 
          accattivante, coinvolgente. Servita da un cast vocale che rappresenta 
          il risultato migliore dello spettacolo.</p>
        <span> 
        <button class="view_more">CHIUDI</button>
        </span> </div>
    </div>
  </div>
    </div>

    <div id="messaggero" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Corriere Della Sera</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/messaggero.gif"> 
        <h4 class="modal-title">Mercoledì 26 Novembre 2008</h4>
        <img src="imago/messaggero.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>


	<div id="messaggero1" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Me Messaggero</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/messaggero1.gif"> 
        <h4 class="modal-title">Lunedì 8 Dicembre 2008 </h4>
        <img src="imago/messaggero.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>

    <div id="metro1" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Metro</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/metro1.jpg"> 
        <h4 class="modal-title">Giovedì 27 Novembre 2008 </h4>
        <img src="imago/metro.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>

    <div id="dnews1" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">dnews</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/dnews1.jpg"> 
        <h4 class="modal-title">Giovedì 27 Novembre 2008 </h4>
        <img src="imago/dnews.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>

	<div id="corlazi" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">il corriere laziale</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/corlazi.jpg"> 
        <h4 class="modal-title">Giovedì 27 Novembre 2008 </h4>
        <img src="imago/corlazi1.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>
	<div id="romanist" class="modal fade" role="dialog">
        
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content modal_text_details"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">il Romanista</h4>
      </div>
      <div class="modal-body acst-details"> <img src="imago/romanist.jpg"> 
        <h4 class="modal-title">Giovedì 30 Novembre 2008 </h4>
        <img src="imago/romanista.jpg" class="img-responsive"> </div>
    </div>
  </div>
    </div>
 <div class="container-fluid  no-pad">
  <img src="imago/press1200x350.jpg" class="img-responsive">
 </div>

 <div class="container performer-box press-review">
<div class="intro_com1">
    <h1></h1>
  </div>
  <ul class="clearfix">
    <li><a href="#" data-toggle="modal" data-target="#grafica">
      <img src="imago/la_repubblica.gif">
    </a></li>
    <li><a href="#" data-toggle="modal" data-target="#corsera">
      <img src="imago/corsera.gif">
    </a></li>
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#giornale">
      <img src="imago/giornale.gif">
    </a></li>
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#messaggero1">
      <img src="imago/messaggero1.gif">
    </a></li>
  </ul>
  <div class="intro_com1">
    <h1 ></h1>
  </div>
  <ul class="clearfix">
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#metro1">
      <img src="imago/metro1.jpg">
    </a></li>
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#dnews1">
      <img src="imago/dnews1.jpg">
    </a></li>
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#corlazi">
      <img src="imago/corlazi.jpg">
    </a></li>
    <li><a href="#"><a href="#" data-toggle="modal" data-target="#romanist">
      <img src="imago/romanist.jpg">
    </a></li>
  </ul>
</div>

<?php include 'footer.php';?>
</body>

</html>
