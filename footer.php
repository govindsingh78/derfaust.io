<?php
session_start();
$lang = $_SESSION['lang'];
if($lang == 'en'){
?>
<div class="footer_part">
<footer>
	<div class="container custom_bg1 ">
		<div class="row">
			<div class="col-md-6 no-pad">
				<p>Copyright © 2019 • MUSIKOHL<br>
				All Rights Reserved • <span><a href="privacy-policy.php?lang=en"> PRIVACY POLICY </a></span></p></div>
			<div class="col-md-6 text-right">
				<div class="social_icon">
					<a href="https://www.facebook.com/faust2019/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>
</div> 	
 <?php
} else if($lang == 'it'){
?>
<div class="footer_part">
<footer>
	<div class="container custom_bg1 ">
		<div class="row">
			<div class="col-md-6 no-pad">
				<p>Copyright © 2019 • MUSIKOHL<br>
				All Rights Reserved • <span><a href="privacy-policy-itl.php?lang=it"> PRIVACY POLICY </a></span></p></div>
			<div class="col-md-6 text-right">
				<div class="social_icon">
					<a href="https://www.facebook.com/faust2019/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
</footer>
</div> 	

 <?php
    }
 ?>




<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootsnav.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});

function languageSwitcher(lang){
var mystr = '<?php echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']; ?>';

//Splitting it with : as the separator

if(lang == 'it'){

//check if url have itl or not -itl.php?lang=it

	if(mystr.indexOf('itl') > -1){
		var myarr = mystr.split("-itl.php?");
		var myvar = myarr[0];
		window.location.href= myvar+"-itl.php?lang="+lang;

	}else{
			var myarr = mystr.split(".php?");
			var myvar = myarr[0];
			window.location.href= myvar+"-itl.php?lang="+lang;


	}



}else{

//else it will be english .php?lang=en

	if(mystr.indexOf('itl') < 0){
		var myarr = mystr.split(".php?");
		var myvar = myarr[0];
		window.location.href= myvar+".php?lang="+lang;

	}else{
			var myarr = mystr.split("-itl.php?");
			var myvar = myarr[0];
			window.location.href= myvar+".php?lang="+lang;


	}

}


//Then read the values from the array where 0 is the first
//Since we skipped the first element in the array, we start at 1
 console.log(myvar+"?lang="+lang);
}
</script>