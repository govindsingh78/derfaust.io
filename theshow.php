<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Show</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/top_bg1.png" class="img-responsive">
 </div>
 <div class="container content_box">
<div class="intro_com">
 	<div class="row">
	 	
      <h1><span>Introduction</span> Lo Spettacolo</h1>
	 	<div class="col-md-6 no-pad">
			<p>
			This musical was born from the idea of ​​bringing on stage a masterpiece of German romanticism ; as Goethe himself said, this work is " <span class="rd_clr">incommensurable</span> " and is the result of a work that the German writer developed during the entire span of his life, making the last revision, only a few months before his death.</p>

			<p>Conceived and realized through a few years of passionate and passionate work, with the help of 2 artists who have been able to make the best of my realization ideas: the sensitive composer <span class="rd_clr">M ° Roberto CHIOCCIA</span> and the refined and eclectic librettist <span class="rd_clr">Alessandro HELLMANN</span>. To them goes my thanks for having worked so well and in such an inspired way, to allow me the "risk" of this enterprise, fruit above all of great love for the theater in general and for the musical. It's a <span class="rd_clr">great POPULAR FABLE</span> , serious but not serious! In this sense it is  <span class="rd_clr">enjoyable by viewers of all</span> ages and cultures, with pop music, veined and influenced by classic Italian opera;

			The COREOGRAPHS , beautiful and engaging, were made by Stefano BONTEMPI.

			The CAST is the result of a long and meticulous selection work that has produced a company in which young talents act.</p>
		</div>

		<div class="col-md-6 no-pad">
			<div class="har_shop_al_item_bl">
	            <a href="#" class="har_shop_al_item">
	                <span class="har_shop_item_disk">
	                    <span class="har_shop_item_cover har_image_bck" data-image="images/video.png" class="img-responsive" style="background-image: url(&quot;images/video.png&quot;);" class="img-responsive"></span>
	                    <span class="har_shop_item_vinyl">
	                        <span class="har_shop_item_vinyl_img har_image_bck" data-image="images/video.png" class="img-responsive" style="background-image: url(&quot;images/video.png&quot;);" class="img-responsive"></span>
	                        <span class="har_shop_item_vinyl_hole"></span>
	                    </span>
	                </span>
	            </a>
        	</div>
    	</div>
	</div>
</div>
 <div class="intro_com1">
 	<div class="row">
	 	
      <h1>The Philosophy<span> 
        <button class="view_more pull-right">View More</button>
        </span></h1>
	 	<div class="col-md-11 no-pad">
	 			<p>
	 			The story has as its protagonist a scholar, Johann Faust, who, now old, not having found answers to the meaning of life through his studies, is tempted by the demon Mephistopheles and bets his soul in exchange for youth, wisdom and power. </p>
				<p>Now Faust, almighty, can dispose of the fate of others: falls in love with the beautiful Margaret and succeeds (with the help of Mefistofele) to conquer it; Faust causes her to administer a sleeping pill (given by Mephistopheles) to her mother in order to meet. </p>
				<p>The diabolical sleeping pill, however, is actually a powerful poison, which kills the mother.</p>
				<p>Leaving Margherita's house, Faust fights against Valentino, who defends his sister's honor; Faust manages to kill him, helped by the demonic forces of Mephistopheles and escapes with him to avoid capture. </p>
				<p>Margherita, is cursed by her dying brother; abandoned by Faust and desperate because she is pregnant, after childbirth kills the baby (drowning). This is why she is judged, sentenced to death and imprisoned. </p>
				<p>the divine and the human, " <span class="rd_clr">the ability of man to be measure of all things</span> ".</p>
				<h3>Dedicated with affection, to the memory of my dear parents Adelmo and Adele Marco KOHLER</h3>
	 	</div>
	 </div>
</div>
</div>
<?php include 'footer.php';?>
</body>

</html>
