<?php
session_start();
include 'config.php';
$actual_link = "https://" . $_SERVER[HTTP_HOST] . "" . $_SERVER[REQUEST_URI];

// if($language == 'en'){

// if($actual_link != "https://workdemo4u.com/musikohl/index.php?lang=en"){
//         header('Location:  index.php?lang=en');
//         exit;
//     }

// }else if($language == 'it'){

// if($actual_link != "https://workdemo4u.com/musikohl/index-itl.php?lang=it"){
//         header('Location:  index-itl.php?lang=it');
//         exit;
//     }

// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">

                 <?php include 'navbar.php';?>


    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/home_eng.jpg" class="img-responsive">
 </div>
 <div class="container content_box">
<div class="intro_com">
 	<div class="row">
	 	
      <h1><span>Introduction</span> The Desire That Overwhelm the soul!</h1>
	 	<div class="col-md-6 no-pad">
			<p>
	This musical was born from the idea of ​​bringing on stage a masterpiece of German romanticism.<br>
As Goethe himself said, this work is " <span class="rd_clr">INCOMMENSURABLE</span> " and is the result of a work that the German writer developed during the entire span of his life, making the last revision, only a few months before his death.</p>

			<p>Conceived and realized through a few years of passionate and passionate work, with the help of 2 artists who have been able to make the best of my realization ideas: the sensitive composer<br>
 <span class="rd_clr">M ° Roberto CHIOCCIA</span> and the refined and eclectic librettist <span class="rd_clr">Alessandro HELLMANN</span>.<br>
To them goes my thanks for having worked so well and in such an inspired way, to allow me the "risk" of this enterprise, fruit above all of great love for the theater in general and for the musical.<br>
It's a <span class="rd_clr">GREAT POPULAR FABLE</span>, serious but not serious! In this sense it is  <span class="rd_clr">enjoyable by viewers of all ages and cultures</span>, with pop music, veined and influenced by classic Italian opera;<br>
The <b>COREOGRAPHY</b>, beautiful and engaging, were made by <span class="rd_clr">Stefano BONTEMPI</span>.<br>
The <span class="rd_clr">CAST</span> is the result of a long and meticulous selection work that has produced a company in which young talents act.</p>
		</div>
        <section class="har_section har_image_bck" data-color="#f2f2f2" id="featured">
    		<div class="col-md-6 no-pad">
                <div class="har_team_slider">
        			<div class="har_shop_al_item_bl">
        	            <a href="javascript:;" class="har_shop_al_item text-center">
                          <span class="har_shop_item_disk">
                             <img src="imago/video_new.png">
                          </span>
                      </a>
                	</div>
                </div>
        	</div>
        </section>
	</div>
</div>
 <div class="intro_com1">
 	<div class="row">
	 	
      <h1>The Philosophy 
        <!-- <span><button class="view_more pull-right">View More</button></span> -->
      </h1>
	 	 <div class="col-md-11 no-pad">
        <p>
<span class="rd_clr">The FAUST by J.W. Goethe</span> is a universal work that tells of one of man's most profound desires: to understand the ultimate meaning of life and the desire to always overcome oneself in order to reach out to that God, of whom one would be proud to be more than a reflection or shadow ....
In this undisputed masterpiece, Goethe took up the subject of a popular legend popular in Germany and that in England had already been the subject of a theatrical reworking by the Elizabethan poet Christopher Marlowe.<br>

It is said that Goethe had witnessed a puppet show about the history of the doktor Faustus and was impressed and fascinated.<br>

<span class="rd_clr">The SETE of KNOWING</span> is the great theme inherent in Faust; this desire to know, pushes the protagonist to bet his own soul, without any scruple or regret, believing that even Mefistofele will not be able to give him a "moment" so beautiful, to let him accept without regret the eternal damnation ...<br>

Faust can be considered a myth of the modern age: he is a scientist, dissatisfied with the limits of human knowledge, who bets his soul in exchange for love, youth, power and knowledge of the secrets of life.<br>

But Goethe, unlike other authors (eg Marlowe) does not see in Faust the great sinner as popular tradition wanted it.<br>

For this great poet and man of letters, Faust's desire to know, to go further, (<span class="rd_clr">"STREBEN"</span>) is antithetical to the philosophy of enjoyment in itself (<span class="rd_clr">"GENIEßEN"</span>), of Mephistopheles, which hinders the push towards a higher knowledge and therefore God eventually saves Faust's soul.<br>

Goethe's <b>FAUST</b> represents humanity itself, with its weaknesses and aspirations: its intolerance of the limits of conscience and the attempt to overcome them is for Goethe
 <span class="rd_clr">"the noblest of human aspirations"</span>.<br/>

The work, allegory of human life in the whole range of passions, miseries and moments of greatness, affirms the right and the ability of the individual to want to know the divine and the human,  <span class="rd_clr">"the ability of man to be measure of all things "</span>.

</p>


 

<br>
<br>
</div>
	 </div>
</div>
</div>


<!-- Modal Starts-->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false" style="top: 20%">
    
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <h4 class="modal-title">Privacy Policy</h4>
      </div>
      <div class="modal-body" style="padding: 10px;"> 
        <p>This site uses cookies to improve services and experience of readers. 
          If you decide to continue browsing we consider that you accept their 
          use. </p>
      </div>
      <div class="modal-footer"> <a   id="permit" onclick="func1()" class="btn btn-primary">OK</a> 
        <a href="privacy-policy.php?lang=en" target="_blank" class="btn btn-info">More 
        Info</a> </div>
    </div>
  </div>
  </div>
<!-- Modal Ends-->



<?php include 'footer.php';?>


<script type="text/javascript">

    function func1(){
         localStorage.setItem('popState','musikohl1');
         console.log("Clicked Me Already !!");
          $("#myModal").modal('hide');

     }

    $(document).ready(function(){

        if(localStorage.getItem('popState') == 'musikohl1'){
            $("#myModal").modal("hide");

        }

        if(localStorage.getItem('popState') !== 'musikohl1'){
          $("#myModal").modal("show");

        }

    });
</script>

</body>

</html>
