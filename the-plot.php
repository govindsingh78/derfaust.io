<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Plot</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/theopera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container the_plot">
<div class="intro_com">
    <h1>The plot<span>Introduction</span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>The protagonist of the story is a scholar, Johann Faust, who, now old, not having found answers to the meaning of life through his studies, is tempted by the demon Mephistopheles and bets his soul in exchange for youth, wisdom and power. <br>
Now Faust, almighty, can dispose of the fate of others: falls in love with the beautiful Margaret and succeeds (with the help of Mephistopheles) to conquer it;<br>
Faust causes her to administer a sleeping pill (given by Mephistopheles) to her mother in order to meet. The diabolical sleeping pill, however, is actually a powerful poison, which kills the mother.<br>
Leaving Margherita's house, Faust fights against Valentino, who defends his sister's honor; Faust manages to kill him, helped by the demonic forces of Mephistopheles and escapes with him to avoid capture. <br>
Margherita, is cursed by her dying brother; abandoned by Faust and desperate because she is pregnant, after childbirth kills the baby (drowning).<br>
This is why she is judged, sentenced to death and imprisoned.<br>
In the meantime Faust lives new adventures with Mephistopheles: he is led to a Sabbath (the council of witches and demonic powers), but during his performance appears as a vision, the figure of Margaret in prison.
Faust and Mephistopheles then go to jail to free her, but Margaret refuses her freedom, feeling that her salvation is the work of a demon and chooses to face torture. Faust then tears his bet with Mephistopheles, who makes him return old.<br>
Desperate runs to the gallows of Margaret, to be forgiven and despite being back old, is recognized by Margaret with the eyes of love (which never grow old) and gets on the gallows, agreeing to burn with her. <br>
Even the Omnipotent recognizes the desire for good and for knowledge which was at the origin of so much sinning and redeeming them.<br>
</p><br>

<center><h5><b><span class="rd_clr">Dedicated with affection, to the memory of my dear parents Adelmo and Adele</span></b></h5></center>
<center><h5><b><span class="rd_clr">Marco KOHLER</span></b></h5></center><br>

    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
