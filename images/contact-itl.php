<?php
session_start();
include 'config.php';
require_once "phpmailer/class.phpmailer.php";

if (isset($_POST['submit'])) {

    $fullname = $_POST['fname'] . " " . $_POST['lname'];
    $phone = $_POST['tel'];
    $email = $_POST['email'];
    $msg = $_POST['message'];

    $message = '<html><head><title>Contact Message - Musikohl</title></head><body>';
    $message .= "<h2>Contact Info - Musikohl</h2>";
    $message .= "<h3>Following are the detail of visitor</h3>";
    $message .= "<p>Full Name : $fullname </p>";
    $message .= "<p>Contact No. : $phone </p>";
    $message .= "<p>Contact Email : $email </p>";
    $message .= "<p>Message : $msg </p>";
    $message .= "</body></html>";

    // php mailer code starts
    $mail = new PHPMailer(true);
    $mail->IsSMTP(); // telling the class to use SMTP

    $mail->SMTPDebug = 0; // enables SMTP debug information (for testing)
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->SMTPSecure = "ssl"; // sets the prefix to the servier
    $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
    $mail->Port = 465; // set the SMTP port for the GMAIL server

    $mail->Username = 'dev.shubhashish@gmail.com';
    $mail->Password = 'sgfdgtklkoyhcezc';

    $mail->SetFrom($email, 'Musikohl Visitor');
    $mail->AddAddress('info@derfaust.it');
    $mail->AddCC('produkohl@derfaust.it');

    $mail->Subject = trim("Contact Message - Musikohl");
    $mail->MsgHTML($message);

    try {
        $mail->send();

    } catch (Exception $ex) {
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contatto</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
     <style type="text/css">

.error-show{
  color: red !important;
}

</style>
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                      <input type="text" class="form-control" placeholder="Search">
                      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                  </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">

             <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="contact-map">
  <img src="imago/contatti1200x350.jpg">
   <div class="container">
    <!--  <h3>Contatti</h3> -->
   </div>
 </div>
 <div class="container contact-sec clearfix">
   <div class="contact_left">
     <h3>Mettiti in contatto
       <p> Si prega di compilare il modulo !!</p>
        <?php
if (isset($_POST['submit'])) {
    ?>
          <p style='color: green'>
         Grazie per averci contattato. Torneremo molto presto !!
          </p>
          <?php
}
?>
     </h3>
   <form action="" method="post" name="form-contact" id="form-contact" enctype="multipart/form-data" class="clearfix">
      <div class="group">
        <input type="text" id="fname" name="fname" value="" required="required">
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Nome di battesimo</label>
      </div>
       <div class="group">
         <input type="text" id="lname" name="lname" value="" required="required">
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Cognome</label>
      </div>
      <div class="group">
         <input type="text" id="tel" name="tel" value="" required="required">
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Numero di telefono</label>
      </div>
     <div class="group">
         <input type="text" id="email" name="email" value="" required="required">
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>E-mail</label>
      </div>
      <div class="group message-box">
      <textarea class="vwt-Input" id="message" name="message" required="required"></textarea>
        <span class="vwt-Highlight"></span>
        <span class="bar"></span>
        <label for="" class="vwt-Input">Messaggio</label>
    </div>

      <button name="submit" id="submit" class="fa fa-paper-plane"></button>
    </form>
   </div>
   <div class="contact_right">
     <h3>Informazioni sui contatti</h3>
     <div class="clearfix">
       <img src="imago/pin.png">
       <p>Rome</p>
     </div>
      <div class="clearfix">
       <img src="imago/phone.png">
       <p><a href="#">(+123) 123-456-7980</a></p>
     </div>
      <div class="clearfix">
       <img src="imago/mailicn.png">
       <p><a href="#">info@derfaust.it</a></p>
     </div>
     <img class="c-logo" src="imago/logo-contact.png">
   </div>
 </div>
<?php include 'footer.php';?>

<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {



 $('#form-contact').validate({
        ignore: [],
        errorElement: 'div',
        errorClass: 'error-show',
        focusInvalid: false,
        rules:
        {
          "fname": {
            required: true,
          },
          "lname": {
            required: true,
          },
          "tel": {
            required: true,
          },

          "email": {
            required: true,
            email: true,
            customemail: true

          },
          "message": {
            required: true,
          }


        },
        messages:
        {
          "fname": {
            required: "Si prega di fornire il nome",
          },
          "lname": {
            required: "Si prega di fornire il cognome",
          },
          "tel": {
            required: "Si prega di fornire il numero di contatto",
          },
          "email": {
            required: "Si prega di fornire l'indirizzo email!",
            email: "Si prega di inserire un indirizzo email valido",
            customemail: "Si prega di inserire un indirizzo email valido"

          },
          "message": {
            required: "Si prega di fornire i dettagli del messaggio",
          }

        }
    });

    $.validator.addMethod("customemail",
        function(value, element) {

                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);

        },
    "Please enter a valid email !!"
    );




  })
</script>
</body>

</html>
