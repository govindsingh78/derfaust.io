<?php
session_start();
include 'config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gli Autori</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/spettacolo1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
    <h1>IL COMPOSITORE<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/roberto.jpg"></span> 
      <h4>Roberto CHIOCCIA</h4>
    </div>
    <div class="characters_right_box">
      <h3>Roma <abbr>(1963)</abbr></h3>
      <p>La sua figura professionale si forma attraverso una completa preparazione artistica, sia di tipo figurativo che specificatamente musicale.<br><br>

Il liceo artistico e il conservatorio di S.Cecilia in Roma, nel quale frequenta i corsi di pianoforte, composizione e clarinetto, sono i luoghi dove si manifesta ben presto quella spiccata dote di sensibilità che lo contraddistingue nello svolgimento attuale dei suoi lavori.<br><br>

Negli anni compresi tra il 1980 e il 1990, partecipa attivamente in organici orchestrali e bandistici, che, insieme alle esperienze di musica acquisite dal vivo, diventano materia prima di quella tavolozza timbrica alla quale un musicista attinge per tutta la vita.<br><br>

Compositore SIAE fin dal 1982, ha al suo attivo prodotti musicali classici e moderni già editi e distribuiti da varie etichette discografiche.Scrive musica per colonne sonore di documentari e film:<br>

<ul class="cinema_box">
          <li>“Ultima maremma” di A.Bàssan -<span>Premio Nastro d'argento - Taormina 1983</span></li>
          <li>“Gratta e vinci” Dània Film 1998 - ediz. EMI</li>
           <li>Documentario “I trabucchi” - produzione Parco nazionale del Gargano</li>
        </ul>
        <p>In teatro collabora per spettacoli di varietà (Teatro delle attrattive 1987) e per commedie musicali (Teatro della Cometa, Teatro Sala Umberto).<br><br>
Come musicista arrangiatore segue molti artisti nella realizzazione dei loro cd, curando la orchestrazione, le registrazioni in studio e l'intero percorso creativo.
<br>Attualmente oltre alla preparazione di nuovi musical e prodotti cinematografici di animazione, ha al suo attivo realizzazioni internazionali collaborando con radio statunitensi e autori e produttori 
di Belgio, Grecia, Polonia, Francia e Russia.</p>
      
    </div>
  </div>
  <div class="intro_com1">
    <h1>IL LIBRETTISTA<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/aless.jpg"></span> 
      <h4>Alessandro HELLMANN</h4>
    </div>
    <div class="characters_right_box">
      <h3>Genova <abbr>(1971)</abbr></h3>
      <p>Si è guadagnato lo status di autore di culto, spaziando dalla letteratura alla musica e al teatro con lavori di rara intensità e coerenza.<br><br>
        <p>La sua opera letteraria descrive il punto di vista dei più deboli con uno stile umorale e personalissimo, fatto di improvvise verticalizzazioni liriche, sempre in equilibrio tra il registro drammatico e il grottesco.</p><br>
        <p>Tra le sue opere citiamo, tra le altre, titoli come <span>“JUSTUS” (2017)</span>, e <span>“DAVID LAZZARETTI” (2013)</span>, fonte di  ispirazione per la piéce teatrale  di <b>Simone Cristicchi</b>, <span>“CUBA” (2008)</span> e il pluri-premiato <span>"CENT’ANNI DI VELENO” (2005)</span>, pubblicato in Francia come <span>“LE FLEUVE PILLÉ”</span>, recensito dalla stampa nazionale come <b>"un piccolo capolavoro di tecnica narrativa"</b> e 
		<b>"un testo di grande valore letterario e umano"</b>.<br><br>

Ha anche scritto per il teatro e collabora, in qualità di compositore, con diversi musicisti e gruppi emergenti avendo inciso un paio di album: <span>“SUMMERTIME BLUE” (Trelune, 2008)</span> e <span>“COME PRATI A PRIMAVERA” (Maremosso, 2019)</span><br><br>

Ha ricevuto diversi riconoscimenti, tra i quali, il prestigioso <span>PREMIO FABRIZIO DE ANDRÉ</span> <b>“come miglior autore”</b>, consegnatogli da <b>DORI GHEZZI</b>.</p><br><br>
		
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
