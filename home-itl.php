<?php
session_start();
include 'config.php';

$actual_link = "https://" . $_SERVER[HTTP_HOST] . "" . $_SERVER[REQUEST_URI];

// if($language == 'en'){

// if($actual_link != "https://workdemo4u.com/musikohl/index.php?lang=en"){
//        header('Location:  index.php?lang=en');
//         exit;
//     }

// }else if($language == 'it'){

// if($actual_link != "https://workdemo4u.com/musikohl/index-itl.php?lang=it"){
//        header('Location:  index-itl.php?lang=it');
//         exit;
//     }

// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">



                <?php include 'navbar.php';?>


    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/home.jpg" class="img-responsive">
 </div>
 <div class="container content_box">
<div class="intro_com">
 	<div class="row">
	 	
      <h1><span>Introduzione</span> IL Desiderio che Travolge L'Anima !</h1>
	 	<div class="col-md-6 no-pad">
			<p>
            Questo Musical è nato dall'idea di portare in scena un capolavoro del romanticismo Tedesco; come lo stesso Goethe ebbe a dire, quest'opera è" <span class="rd_clr">incommensurabile</span> "  ed è frutto di un lavoro che lo scrittore tedesco sviluppò durante l'intero arco della sua esistenza, apportando l'ultima revisione, solo pochi mesi prima della morte.</p>

            
        <p>Concepito e realizzato attraverso alcuni anni di lavoro appassionato 
          e appassionante, con l'aiuto di 2 artisti che hanno saputo rendere al meglio le mie idee realizzative: il sensibile compositore <span class="rd_clr">M°Roberto 
          CHIOCCIA</span> ed il raffinato ed eclettico librettista<span class="rd_clr"> 
          Alessandro HELLMANN.</span><br>
            A loro va il mio ringraziamento per aver lavorato così bene e in un modo così ispirato, da consentirmi il "rischio" di questa impresa, frutto sopratutto di grande amore per il teatro in generale e per il musical in particolare.<br>
          E' una <span class="rd_clr">Grande FAVOLA POPOLARE </span>seria ma non 
          seriosa! In questo senso è <span class="rd_clr">godibile da spettatori di ogni età e cultura</span> con musiche pop, venate e influenzate dalla 
          classica opera italiana;<br>
          Le <span class="rd_clr">COREOGRAFIE, </span>belle e coinvolgenti, sono 
          state realizzate da <span class="rd_clr">Stefano BONTEMPI.</span><br>

            Il <span class="rd_clr">CAST </span>è il risultato di un lungo e meticoloso lavoro di selezione che ha prodotto una compagnia in cui agiscono giovani talenti.</p>
		</div>
        <section class="har_section har_image_bck" data-color="#f2f2f2" id="featured">
    		<div class="col-md-6 no-pad">
                <div class="har_team_slider">
        			<div class="har_shop_al_item_bl">
        	            <a href="javascript:;" class="har_shop_al_item text-center">
        	                <span class="har_shop_item_disk">
        	                   <img src="imago/video_new.png">
        	                </span>
        	            </a>
                	</div>
                </div>
        	</div>
        </section>
	</div>
</div>
 <div class="intro_com1">
 	<div class="row">
	 	
      <h1>La Filosofia 
        <!-- <span><button class="view_more pull-right">View More</button></span> -->
      </h1>
	 	<div class="col-md-11 no-pad">
	 			<p>
<span class="rd_clr">Il FAUST di J.W.Goethe</span> è un’opera universale che racconta di uno dei desideri più profondi dell’uomo: comprendere il senso ultimo della vita e la voglia di superarsi sempre per tendere verso quel Dio, del quale orgogliosamente si vorrebbe essere qualcosa di più di un riflesso o di un’ombra....

In questo indiscusso capolavoro, Goethe riprese il soggetto di una leggenda popolare molto diffusa in Germania e che in Inghilterra era già stata oggetto di una rielaborazione teatrale da parte del poeta elisabettiano Christopher Marlowe.<br>
Si dice che Goethe avesse assistito a uno spettacolo di marionette sulla storia del doktor Faustus e ne fosse rimasto colpito e affascinato.<br>
<span class="rd_clr">La SETE di SAPERE</span> è il grande tema insito in Faust; questa voglia di sapere, spinge il protagonista a scommettere la propria anima, senza alcuno scrupolo o rimpianto, credendo che neanche Mefistofele riuscirà a dargli un “attimo” così bello, da fargli accettare senza rimpianti l’eterna dannazione...<br>
Faust può essere considerato un mito dell’età moderna: è uno scienziato, insoddisfatto dei limiti del sapere umano, che scommette la sua anima per avere in cambio amore, giovinezza, potere e conoscenza dei segreti della vita.<br>
Ma Goethe, al contrario di altri autori (ad es. Marlowe) non vede in Faust il grande peccatore come lo voleva la tradizione popolare.<br>
Per questo grande poeta e letterato, la volontà di Faust di sapere, di andare oltre, (<span class="rd_clr">“STREBEN”</span>) è antitetica alla filosofia del godimento fine a se stesso (<span class="rd_clr">“GENIE&szlig;EN”</span>), di Mefistofele, che ostacola la spinta verso una più alta conoscenza e quindi Dio alla fine salva l'anima di Faust.<br>
Il <b>FAUST</b> di Goethe rappresenta l'umanità stessa, con le sue debolezze e aspirazioni: la sua insofferenza dei limiti della coscienza e il tentativo di superarli è per Goethe<br>
 <span class="rd_clr">"la più nobile delle aspirazioni dell'uomo"</span>. L'opera, allegoria della vita umana nell'intera gamma delle passioni, delle miserie e dei momenti di grandezza, afferma il diritto e la capacità dell'individuo di voler conoscere il divino e l'umano, <span class="rd_clr">"la capacità dell'uomo di essere misura di tutte le cose"</span>.

<br>
<br>
</div>
	 </div>
</div>
</div>
<!-- Modal Starts-->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false" style="top: 20%">
    
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <h4 class="modal-title">Privacy Policy</h4>
      </div>
      <div class="modal-body" style="padding: 10px;"> 
        <p>Questo sito utilizza i cookie per migliorare i servizi e l'esperienza 
          dei lettori. Se decidi di continuare la navigazione consideriamo che 
          accetti il ​​loro uso. </p>
      </div>
      <div class="modal-footer"> <a   id="permit" onclick="func1()" class="btn btn-primary">OK</a> 
        <a href="privacy-policy-itl.php?lang=it" target="_blank" class="btn btn-info">Ulteriori 
        informazioni</a> </div>
    </div>
  </div>
  </div>
<!-- Modal Ends-->
 <?php include 'footer.php';?>
<script type="text/javascript">

    function func1(){
         localStorage.setItem('popState','musikohl1');
         console.log("Clicked Me Already !!");
          $("#myModal").modal('hide');

     }

    $(document).ready(function(){

        if(localStorage.getItem('popState') == 'musikohl1'){
            $("#myModal").modal("hide");

        }

        if(localStorage.getItem('popState') !== 'musikohl1'){
          $("#myModal").modal("show");

        }

    });
</script>
</body>

</html>
