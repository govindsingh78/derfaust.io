
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAUST IL MUSICAL!</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link  href="css/font-awesome.min.css" rel="stylesheet">
</head>
<style type="text/css">
body{ background: #000 }
  .splash{background: #000; height: 100%; margin:0px; padding:50px 0px;}
  .log_spc{ margin: 20px 0px; }
</style>
<body>
  <div class="splash">
    <div class="container">
      <div class="row">
        <center>
        <div class="col-md-12"><img src="https://www.derfaust.it/imago/titolo.jpg" class="img-responsive" width="798" height="198" border="0"></div>
        <div class="col-md-12"><a href="https://derfaust.it/home-itl.php?lang=it"><img src="https://www.derfaust.it/imago/0%20titolo_ita1200x97.png" class="img-responsive" width="1200" height="97" border="0"></a></div>
        <div class="col-md-12"><a href="https://derfaust.it/home.php?lang=en"><img src="https://www.derfaust.it/imago/0%20titolo_eng1200x97.png" class="img-responsive" width="1200" height="97" border="0"></a></div>
         <div class="col-md-12 log_spc"><a href="http://www.musikohl.it"><img src="https://www.derfaust.it/imago/musikohl130x85.png" class="img-responsive" width="130" height="85" border="0"></a><font color="#CC0000">&copy; 2008-2019</font></div>
        </center>
      </div>
  </div>
</div>
  <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
</body>

</html>
