<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>La Trama</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="stylesheet" href="css/responsive.css">
      <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
    <div class="cus-nav">
        <nav class="navbar navbar-default bootsnav top_nav">

            <!-- Start Top Search -->
            <div class="top-search">
                <div class="container">
                    
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
                </div>
            </div>
            <!-- End Top Search -->

            <div class="container-fluid top_hdr">
                <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/la_opera1200x350.jpg" class="img-responsive">
 </div>
 <div class="container the_plot">
<div class="intro_com">
    <h1>La Trama<span>Introduzione</span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_right_box">
      <p>La storia ha come protagonista uno studioso, Johann Faust, che, ormai vecchio, non avendo trovato risposte al senso della vita attraverso i suoi studi, viene tentato dal demonio Mefistofele e scommette la propria anima in cambio di giovinezza, sapienza e potere. <br>
Ora Faust, onnipotente, può disporre delle sorti altrui: si innamora della bella popolana Margherita e riesce (con l'aiutino di Mefistofele) a conquistarla; Faust la induce a somministrare un sonnifero (dato da Mefistofele) alla propria madre per potersi incontrare.<br>
Il sonnifero diabolico, però, è in realtà un potente veleno, che uccide la madre.<br>
Uscendo dalla casa di Margherita, Faust si scontra in duello con Valentino, che difende l'onore della sorella; Faust riesce ad ucciderlo, aiutato dalle arti demoniache di Mefistofele e scappa con lui per evitare la cattura.<br>
Margherita, viene maledetta dal fratello morente; abbandonata da Faust e disperata perchè è incinta, dopo il parto uccide il neonato (affogandolo). <br>
Per questo viene giudicata, condannata a morte e incarcerata.<br>
Nel frattempo Faust vive con Mefistofele nuove avventure: viene condotto ad un Sabba (il concilio di streghe e potenze demoniache), ma durante il suo svolgimento appare come in una visione, la figura di Margherita in carcere.<br>
Faust e Mefistofele si recano allora in carcere per liberarla, ma Margherita rifiuta la libertà, sentendo che la sua salvezza sarebbe opera di un demonio e sceglie di affrontare il supplizio. Faust allora straccia la scommessa con Mefistofele, che lo fa tornare vecchio.<br>
Disperato corre verso il patibolo di Margherita, per farsi perdonare e nonostante sia tornato vecchio, viene riconosciuto da Margherita con gli occhi dell'amore (che non invecchiano mai) e sale sul patibolo, accettando di bruciare con lei.<br>
Anche l’Onnipotente riconosce il desiderio di bene e di conoscenza che è stato all'origine di tanto peccare e li redime.
 </p><br>

<center>
        <h5 align="center"><b><span class="rd_clr">Dedicato con affetto, alla 
          memoria dei miei cari genitori Adelmo e Adele</span></b><br>
        </h5>
      </center>
      <h5 align="center"><b><span class="rd_clr">Marco KOHLER</span></b></h5>
      <div align="center"><br>
      </div>
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
