<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>the Cast</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
             <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/show1200x350.jpg" class="img-responsive">
 </div>
 <div id="grafica" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nicola Zamperetti (Valdagno)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer1.jpg"> 
          <h3>(Faust)</h3>
        </div>
        <p><span>- 2004</span> works in the company of P.Insegno in the shows 
          "<span>Insegnami a sognare</span> ", (Teatro Sistina) and " <span> Buonasera 
          Buonasera</span> " (Teatro Parioli) directed by C. Insegno. <br>
          -<span>2004/05</span> is part of the cast of the musical "Joseph and 
          the amazing tunic of dreams in technicolor" by A. Lloyd Webber and T. 
          Rice with Rossana Casale directed by C. Insegno. <br>
          <br>
          -<span> 2006</span> always directed by C.Insegno back to the Teatro 
          Sistina with the show "<span> A bit 'before the first</span> " and is 
          the protagonist of the musical horror dementia "<span>Cannibal</span> 
          "by Trey Parker at the Greek Theater. <br>
          Also in 2006 he is part of the cast of the musical "<span> Grease </span>" 
          of the Compagnia della Rancia, in the role of Doody, and is the cover 
          of Danny and Kenickie. -<span> 2007</span> made his debut in the world 
          premiere of Riz Ortolani's "<span>The Prince of Youth </span> " Musical 
          , in the role of "Poeta Pulci" at the Teatro La Fenice in Venice. In 
          the same year ago "<span> Sulle ali del sogno </span>", directed by 
          F.Draghetti and "<span> Come on cretino </span>"with and by P.Insegno 
          and R.Ciufoli, directed by P.Francesco Pingitore, both at Salone Margherita.</p>
      </div>
    </div>
  </div>
</div>
  <div class="container performer-box">
    <div class="intro_com1">
      
    <h1>The Performer<span></span></h1>
    </div>
    <ul class="clearfix">
      <li><a href="#" data-toggle="modal" data-target="#grafica">
        <img src="imago/pergormer1.jpg">
        <h3>MARCO</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica1">
        <img src="imago/pergormer2.jpg">
        <h3>ENRICO</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica2">
        <img src="imago/pergormer3.jpg">
        <h3>VALENTINA</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica3">
        <img src="imago/pergormer4.jpg">
        <h3>CLAUDIA</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica4">
        <img src="imago/pergormer5.jpg">
        <h3>NICOLA</h3>
      </a></li>
    </ul>
    <div class="intro_com1">
      
    <h1>The Dancers<span></span></h1>
    </div>
    <ul class="clearfix">
      <li><a href="#" data-toggle="modal" data-target="#grafica5">
        <img src="imago/dancers1.jpg">
        <h3>Azzurra</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica6">
        <img src="imago/dancers2.jpg">
        <h3>Silvia</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica7">
        <img src="imago/dancers3.jpg">
        <h3>Laura</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica8">
        <img src="imago/dancers4.jpg">
        <h3>Agata</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica9">
        <img src="imago/dancers5.jpg">
        <h3>Sarah</h3>
      </a></li>
    </ul>
    <ul class="clearfix last_pick">
      <li><a href="#" data-toggle="modal" data-target="#grafica10">
        <img src="imago/dancers6.jpg">
        <h3>Monica</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica11">
        <img src="imago/dancers7.jpg">
        <h3>Sara</h3>
      </a></li>
      <li><a href="#" data-toggle="modal" data-target="#grafica12">
        <img src="imago/dancers8.jpg">
        <h3>Rossella</h3>
      </a></li>
    </ul>
</div>




<?php include 'footer.php';?>


<!-- start popup window section -->

 <!-- start one popup -->
 <div id="grafica" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Marco STABILE (Cassino)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer1.jpg"> 
          <h3>FAUST</h3>
        </div>
        <p>- <span>2004 he</span> works in the company of P.Insegno in the shows 
          " <span>Insegnami a sognare</span> ", (Teatro Sistina) and " <span>Buonasera 
          Buonasera</span> " (Teatro Parioli) directed by C. Insegno.<br>
          <br>
          - <span>2004/05</span> is part of the cast of the musical "Joseph and 
          the amazing tunic of dreams in technicolor" by A. Lloyd Webber and T. 
          Rice with Rossana Casale directed by C. Insegno. <br>
          <br>
          - <span>2006</span> always directed by C.Insegno back to the Teatro 
          Sistina with the show " <span>A bit 'before the first</span> " and is 
          the protagonist of the musical horror dementia "<span>Cannibal</span> 
          "by Trey Parker at the Greek Theater. <br>
          <br>
          Also in 2006 he is part of the cast of the musical " <span>Grease</span> 
          " of the Compagnia della Rancia, in the role of Doody, and is the cover 
          of Danny and Kenickie. <br>
          <br>
          - <span>2007</span> made his debut in the world premiere of Riz Ortolani's 
          " <span>The Prince of Youth</span> " Musical , in the role of "Poeta 
          Pulci" at the Teatro La Fenice in Venice. <br>
          <br>
          In the same year ago " <span>Sulle ali del sogno</span> ", directed 
          by F.Draghetti and " <span>Come on cretino</span>"with and by P.Insegno 
          and R.Ciufoli, directed by P.Francesco Pingitore, both at Salone Margherita.</p>
      </div>
    </div>
  </div>
              </div>
 <!-- end one popup -->

<!-- start two popup -->
 <div id="grafica1" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enrico BERNARDI (Cuneo) </h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer2.jpg"> 
          <h3>MEFISTOFELE</h3>
        </div>
        <p>He trained artistically and specialized in Musical at the <span>Royal 
          Academy of Music in London and the BSMT in Bologna</span> . She played: 
          <br>
          <br>
          • <span>Pilate</span> in "Jesus Christ Superstar" <br>
          <br>
          • <span>Sweeney</span> in "Sweeney Todd", chorister in "Così Fan 
          Tutte" and "Falstaff" <br>
          <br>
          • <span>Napthali and Somellier</span> in "Joseph" with Rossana Casale, 
          directed by C. Insegno <br>
          <br>
          • <span>Audrey II</span> , the voice of the plant in "La Piccola Bottega 
          degli Orrori", directed by F. Bellone <br>
          <br>
          • <span>Chester</span>in "Alta Società", with Vanessa Incontrada, 
          directed by M.Piparo <br>
          <br>
          • <span>Ugolino and Tommaso</span> in "La Divina Commedia" <br>
          <br>
          <span>Vocal Coach</span> of "8 Women and a Mystery" and<span> Music 
          Director</span> of "Ask me if I want the moon" with Justine Mattera, 
          direction C. I teach.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end two popup -->

<!-- start third popup -->
 <div id="grafica2" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Valentina SPALLETTA (Marino)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer3.jpg"> 
          <h3>DAISY</h3>
        </div>
        <p>He had a complete artistic preparation at the C.Pani Academy in Rome. 
          He studied with the Masters, A.Cerliani, C.Pani, AMMerli, C.Insegno, 
          P.Insegno, G. Molinari, D.Pandimiglio, as well as attending several 
          courses of vocal and body movement recognized at national level. <br>
          <br>
          He made his debut at the Teatro Sistina with "Teach me to dream"; "Buonasera 
          Buonasera" at Parioli; "Cannibal the musical"; "A little 'before the 
          first" al Sistina all directed by C.Insegno; "Grease" company of the 
          Rancia Allianz theater in Milan directed by F.Bellone<br>
          <br>
          "Waiting for Barbra" by and with D. Pandimiglio; "The Prince of Youth" 
          by M ° Riz Ortolani, at the Phoenix of Venice, directed by Pier Luigi 
          Pizzi; "On the wings of a dream" at Salone Margherita, directed by F.Draghetti. 
          <br>
          <br>
          In television he arrives with numerous docufiction for the program "Italy 
          on 2" and "I recommended" with P. Insegno.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end third popup -->

<!-- start four popup -->
 <div id="grafica3" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Claudia TORTORICI (Rome)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer4.jpg"> 
          <h3>THE WITCH, MARTA, LILITH</h3>
        </div>
        <p>He began his artistic career as a singer from the meeting with Stefano 
          Zanchetti, a vocal motivational coach, who has been his singing teacher 
          since 1992. <br>
          <br>
          He participates in television broadcasts on regional networks (The Wish 
          List in Teleambiente "The Fan Process" at Teleroma 56 and "I raccomandati" 
          at Rai 1)<br>
          <br>
          In <span>2005</span> he co-stars with the role of <span>SAMIR</span> 
          in the musical " <span>The Miracle of Father Pio</span> "directed by 
          Daniela Pistagna where he also shows off his talents of actress refined 
          with Francesca Viscardi Leonetti assistant of Susan Batson Actor's Studio 
          NYC. <br>
          <br>
          <span>2006</span> Under his direction he stages " <span>Anna Cappelli</span> 
          " by Annibale Ruccello.<br>
          <br>
          This year he has just finished shooting the independent film <span>GIOVANI 
          AVAILABLE</span> by the director Riccardo Camilli in the role of <span>Betta</span> 
          . </p>
      </div>
    </div>
  </div>
</div>
 <!-- end four popup -->

 <!-- start five popup -->
 <div id="grafica4" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nicola ZAMPERETTI (Valdagno)</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/pergormer5.jpg"> 
          <h3>WAGNER, VALENTINO</h3>
        </div>
        <p>He moved his first dance steps at the age of 15 at the ARDOR school 
          in Cornedo Vicentino and later he also studied classical with Antonella 
          Disconzi. <br>
          <br>
          In 1997 after his first audition as a dancer for the RAIUNO program 
          "Faccia Tosta", he moved to Rome where he studied at <span>IALS</span> 
          with Sara Greco , Mauro Astolfi, Roberto Pallara and Marco Garofalo.<br>
          <br>
          In 1997 after his first audition as a dancer for the RAIUNO program 
          "Faccia Tosta", he moved to Rome where he studied at <span>IALS</span> 
          with Sara Greco , Mauro Astolfi, Roberto Pallara and Marco Garofalo. 
          - 1999 He is an actor-dancer in the Colossal " <span>U-571</span> " 
          directed by Jonathan Moscow, and the TV film " <span>Piccolo mondo antico</span> 
          ", directed by Cinzia Torrini.<br>
          Over the years he completed his preparation as a Performer studying 
          acting with Lino Damiani .<br>
          <br>
          - 2006 leads " <span>Voilà</span> " on Sky Leonardo <br>
          <br>
          - 2007 he obtains a scholarship for the course "Strasberg method" at 
          the Konstanlee directed by Ilza Prestinari. <br>
          <br>
          - 2008 is in Fiction " <span>Try again Prof 3</span> " and " <span>Il 
          Sangue e la rosa</span> ". <br>
          <br>
          He works in numerous television programs , including: <br>
          "<span>Sarabanda</span> "," <span>Carramba che fortuna</span> "," <span>Go 
          where your heart takes you</span> ","<span> Domenica IN</span> ",<br>
          <br>
          Also performer in various <span>MUSICAL</span> : " Grease "," The portrait 
          of Dorian Gray "," Giovanna D'arco "," Candido "," Prince of Youth "," 
          Chorus line "," Beggar's opera ". </p>
      </div>
    </div>
  </div>
</div>
 <!-- end five popup -->

 <!-- start six popup -->
 <div id="grafica5" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Azzurra CACCETTA</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers1.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>He studied classical, contemporary and dance theater in Lecce with 
          the teachers <span>Ioan Iosif Girba</span> (first dancer of the Budapest 
          opera), Enza Curto, Michele Abbondanza. <br>
          <br>
          After moving to Rome, she deepened the Modern-Contemporary and Hip-Hop 
          styles with the masters <span>Astolfi and Silgoner</span> ; in addition 
          she also has experience as an actress-singer studying with the masters 
          Claudio Boccaccini, Gianluca Ferrato, Rossana Casale, Raffaella Misiti. 
          <br>
          <br>
          She takes part as a dancer-actress-singer in several musicals, video-clips, 
          TV programs, films. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end six popup -->

 <!-- start Seven popup -->
 <div id="grafica6" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Silvia FLORIDI</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers2.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>In Television he participates in:<br>
          <br>
          "Numero Uno", " <span>Furore</span> ", "Mother's Day", "Good luck", 
          "For life", as well as commercials and cinematographic parts ( <span>APRIL 
          and PINOCCHIO</span> ). <br>
          <br>
          Among the most significant theatrical experiences we remember: <br>
          <br>
          “<span>A pair of wings</span>”,<br>
          “<span>They are playing our song</span>”<br>
          ”<span>Promises, promises</span>”,<br>
        </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Seven popup -->

 <!-- start Eight popup -->
 <div id="grafica7" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Laura Di BIAGIO</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers3.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>Graduated in classical, contemporary and jazz dance at the Centro Studi 
          Danza " Tersicore ", she continues to study in Italy and abroad; he 
          also dedicates himself to the musical disciplines, graduating from the 
          " <span>Musical Theater Academy </span>". <br>
          <br>
          Professionally she is engaged in cinema and television, but her passion 
          is theater. She performs as a dancer, actress, singer in Italy and abroad, 
          with various companies of dance, theater and musicals. <br>
          <br>
          She is currently working at the Sistina for the musical " <span>The 
          Seven Kings of Rome</span> " and with the companies "The barefoot dancers", 
          "<span>Open Dance Theater</span> "and" Incontempo "for dance.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Eight popup -->

 <!-- start Nine popup -->
 <div id="grafica8" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agata MOSCHINI</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers4.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>He studied classical dance for 3 years, with teachers such as Sabino 
          Rivas and Victor Litinov. He is a teacher at the CSD of A. Paganini 
          and participates in various television shows, including " Telethon ", 
          "Uno Mattina", " <span>Sognando Hollywood</span> " with Matilde Brandi 
          and "Vita da paparazzi" with P.Pingitore. He approaches the theater 
          with Gianni Salvo at the "Piccolo Teatro" of Catania and participates 
          in " <span>Distretto di Polizia 5</span> ". She was a dancer-actress 
          at the Olympic Theater in " <span>La vedova allegra</span> " white "and" 
          <span>Cincillà</span> "and" <span>Come on come folks!</span> "with 
          M.Paulicelli.We participated in the film" Tutta la vita davanti "by 
          P.Virzì and" No problem "with V.Salemme.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end nine popup -->

 <!-- start Ten popup -->
 <div id="grafica9" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sarah POLITO</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers5.jpg"> 
          <!--<h3>(Faust)</h3>-->
        </div>
        <p>He studied classical dance for 3 years, with teachers such as Sabino 
          Rivas and Victor Litinov. He is a teacher at the CSD of A. Paganini 
          and participates in various television shows, including " Telethon ", 
          "Uno Mattina", " <span>Sognando Hollywood</span> " with Matilde Brandi 
          and "Vita da paparazzi" with P.Pingitore. He approaches the theater 
          with Gianni Salvo at the "Piccolo Teatro" of Catania and participates 
          in " <span>Distretto di Polizia 5</span> ". She was a dancer-actress 
          at the Olympic Theater in " <span>La vedova allegra</span> " white "and" 
          <span>Cincillà</span> "and" <span>Come on come folks!</span> "with 
          M.Paulicelli.We participated in the film" Tutta la vita davanti "by 
          P.Virzì and" No problem "with V.Salemme.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Ten popup -->

 <!-- start Elaven popup -->
 <div id="grafica10" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Monica RA MIRES</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers6.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p>Ballerina, actress, singer has participated in theatrical performances, 
          among which, lately, " <span>Epidemia</span> " and " <span>Anja</span> 
          " directed by Claudio Boccaccini, "Pericles prince of Tire" directed 
          by Riccardo Reim, and the musical " <span>Dust when the thought dances</span> 
          " direct by R. Capitani, " <span>Alice in the dance world</span> " directed 
          by Silvia Perelli. <br>
          <br>
          Dancer in RAI and Mediaset television programs: " <span>A record for 
          the summer</span> ", "La sai l'ultima", "For life", "Seven Show", "Girofestival", 
          " <span>Sarabanda</span> ". <br>
          <br>
          Videoclips dancer. </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Elaven popup -->

 <!-- start Twel popup -->
 <div id="grafica11" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sara TELCH</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers7.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p>He began his dance studies at the age of five, graduating in 2002 at 
          the Balletto di Roma.<br>
          <br>
          He works in the musical "<span> Rodolfo Valentino</span> ", " <span>The 
          Phantom of the Opera</span> ", " Profondo Rosso ", and in the musical 
          " <span>Fame</span> " in the role of Carmen Diaz. <br>
          <br>
          Participates as a dancer in the television program " <span>Friends</span> 
          " by Maria De Filippi.</p>
      </div>
    </div>
  </div>
</div>
 <!-- end Twel popup -->

 <!-- start Thirteen popup -->
 <div id="grafica12" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content"> 
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rossella PUGLIESE</h4>
      </div>
      <div class="modal-body acst-details"> 
        <div> <img src="imago/dancers8.jpg"> 
          <!--<h3>(Faust)</h3>  -->
        </div>
        <p> Graduated in 2004 in classical, modern and contemporary dance at the 
          '' <span>British Art of London</span> '. <br>
          <br>
          In 2005 he graduated at <span>the AID (Italian Association of Dancers)</span> 
          in jazz, hip hop. <br>
          <br>
          In 2008 she graduated from the theater academy 'La Stazione' by Claudio 
          Boccaccini. <br>
          <br>
          Among his latest works: <span>Arbaith ; No</span> <br>
          <br>
          <span>smoking</span> ; Goodbye, perhaps never;<br>
          <br>
          <span>Nostos</span> by C.Boccaccini; <br>
          <br>
          <span>Mamma Napoli</span> of E.De Martino </p>
      </div>
    </div>
  </div>
</div>
 <!-- end Thirteen popup -->









 <!-- end popup window section -->



</body>

</html>
