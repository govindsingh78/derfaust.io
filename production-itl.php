<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>La Produzione</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
              <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/spettacolo1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
    <h1>La Produzione<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/marco.jpg"></span> 
      <h4>Marco KOHLER</h4>
    </div>
    <div class="characters_right_box">
      <h3>Roma <abbr>(1955)</abbr></h3>
     <p>Sono approdato al Teatro e alla recitazione nel 1987, quasi per caso; dopo alcune esperienza in una parrocchia dell'EUR, ho iniziato a produrre spettacoli, fondando una compagnia "I GIULLARI" con la quale ho avuto la ventura di portare in scena diversi spettacoli, tra i quali:</p>
<ul class="cinema_box">
          <li><span>L'ARMATA BRANCALEONE</span>nel 1993</li>
          <li><span>I SETTE RE DI ROMA  </span> nello stesso 1993 e sino al 1995</li>
          <li><span>L'ODISSEA </span>  nel 1997</li>
          <li><span>ALLELUJA BRAVA GENTE </span>nel 1999</li>
          <li><span>TOSCA </span>nel 2000</li>
        </ul>
       <p>Dopo questo intenso periodo produttivo, mi sono allontanato per alcuni anni dalle scene, per una sorta di ripensamento sul modo di lavorare, ma nel 2006, il "virus" che chiunque abbia calcato un palcoscenico, sa essere immortale, è tornato vivo e si è rinnovata l'urgenza di produrre un'opera che sin dal lontano 1989, mi aveva colpito e affascinato: <span class="rd_clr">  IL FAUST di Goethe.</span> <br><br>

Dopo un periodo di fervido lavoro, a stretto contatto col compositore Roberto<span> CHIOCCIA  </span>e il fecondo librettista Alessandro <span class="rd_clr"> HELLMANN </span>, produssi questo musical che ritenevo e ritengo sia di grande qualità, in un panorama teatrale difficile e povero di idee, come quello di allora e forse ancor di più oggi, nel momento attuale. <br><br>

Spero che la riedizione cui sto lavorando e che dovrebbe vedere la luce tra il 2019 e il 2020, confermi l'ottima considerazione che sia il pubblico che la stampa ci avevano riservato nel 2008. <br><br>

Anche in questa riedizione speriamo che il pubblico continui, come nel passato, a percepire la passione (ci abbiamo messo tutti l'ANIMA e non poteva essere altrimenti, visto il soggetto) ma anche la grande preparazione e professionalità di tutti gli artisti/e coinvolti. <br><br>

A presto in teatro!</p>
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
