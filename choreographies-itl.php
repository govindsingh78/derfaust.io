<?php
session_start();
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Coreografie</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsive.css">
     <link rel="shortcut icon" href="imago/favicon.ico" />
</head>

<body>
<!-- Start Navigation -->
  <div class="cus-nav">
      <nav class="navbar navbar-default bootsnav top_nav">

          <!-- Start Top Search -->
          <div class="top-search">
              <div class="container">
                  
    <div class="input-group"> <span class="input-group-addon"><i class="fa fa-search"></i></span> 
      <input type="text" class="form-control" placeholder="Search">
      <span class="input-group-addon close-search"><i class="fa fa-times"></i></span> 
    </div>
              </div>
          </div>
          <!-- End Top Search -->

          <div class="container-fluid top_hdr">
            <?php include 'navbar.php';?>

    <div class="clearfix"></div>
 <div class="container-fluid  no-pad">
 	<img src="imago/spettacolo1200x350.jpg" class="img-responsive">
 </div>
 <div class="container">
<div class="intro_com1">
    <h1>Le Coreografie<span></span></h1>
  </div>
  <div class="characters_box clearfix">
    <div class="characters_left_box"> <span> <img src="imago/sreafono.jpg"></span> 
      <h4>Stefano BONTEMPI</h4>
    </div>
    <div class="characters_right_box">
      <h3>Roma <abbr>(1970)</abbr></h3>
     <h5>Si è diplomato all’Accademia Nazionale di Danza Classica di Roma e ha lavorato in Teatro come performer in:</h5>
<ul class="cinema_box">
          <li><span>La Compagnia Stella di Amburgo</span> (Cats)</li>
          <li><span>La Compagnia della Rancia</span> (La cage aux follles, Cabaret)</li>
          <li><span>Con G.Guidi e M.L.Baccarini </span> (Taxi a 2 piazze, Promesse, promesse, Chicago). </li>
        </ul>
        <h5>Ha firmato diverse coreografie, tra cui:</h5>
<ul class="cinema_box">
          <li>“Stanno suonando la nostra canzone”</li>
          <li>“Promesse, promesse”</li>
          <li>“Gianburrasca”</li>
          <li>“Fame”</li>
          <li>“Profondo Rosso”</li>
          <li>“Se stasera sono qui” con  <span>Loretta Goggi.</span> </li>
          <p>In Televisione ha partecipato a “Uno su cento”, “Serata d’onore” e “Colorado”.</p>
          <h5><span>Assistente Coreografo:</span> Ursula DE NITTIS</h5>
        </ul>
    </div>
  </div>

</div>

<?php include 'footer.php';?>
</body>

</html>
